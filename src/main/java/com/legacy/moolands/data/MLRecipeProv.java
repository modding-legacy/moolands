package com.legacy.moolands.data;

import static com.legacy.moolands.registry.MoolandsBlocks.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.annotation.Nullable;

import com.legacy.moolands.MoolandsMod;

import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.PackOutput;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.RecipeOutput;
import net.minecraft.data.recipes.RecipeProvider;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.data.recipes.SingleItemRecipeBuilder;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.SlabBlock;

@SuppressWarnings("unused")
public class MLRecipeProv extends RecipeProvider
{
	private RecipeOutput con;
	private String hasItem = "has_item";

	public MLRecipeProv(PackOutput output, CompletableFuture<HolderLookup.Provider> lookupProvider)
	{
		super(output, lookupProvider);
	}

	@Override
	protected void buildRecipes(RecipeOutput consumer)
	{
		this.con = consumer;

		// TODO stew
		simple2x2(RecipeCategory.BUILDING_BLOCKS, moo_rock, moo_rock_bricks, 4);
		stoneCutting(RecipeCategory.BUILDING_BLOCKS, moo_rock, List.of(moo_rock_bricks));
		slabsStairsWalls(RecipeCategory.BUILDING_BLOCKS, moo_rock, moo_rock_slab, moo_rock_stairs, moo_rock_wall, true);
		slabsStairsWalls(RecipeCategory.BUILDING_BLOCKS, moo_rock_bricks, moo_rock_brick_slab, moo_rock_brick_stairs, moo_rock_brick_wall, true);

		simple2x2(RecipeCategory.BUILDING_BLOCKS, blue_mushroom, blue_mushroom_block);
		simple2x2(RecipeCategory.BUILDING_BLOCKS, yellow_mushroom, yellow_mushroom_block);

		oreSmelting(consumer, List.of(moo_rock_iron_ore.asItem()), RecipeCategory.MISC, Items.IRON_INGOT, 0.7F, 200, "iron_ingot");
	}

	private void simple2x2(RecipeCategory category, ItemLike input, ItemLike output, int amount)
	{
		ShapedRecipeBuilder.shaped(category, output, amount).define('#', input).pattern("##").pattern("##").unlockedBy(hasItem, has(input)).save(con);
	}

	private void simple2x2(RecipeCategory category, ItemLike input, ItemLike output)
	{
		simple2x2(category, input, output, 1);
	}

	private void simple3x3(RecipeCategory category, ItemLike input, ItemLike output, int amount)
	{
		ShapedRecipeBuilder.shaped(category, output, amount).define('#', input).pattern("###").pattern("###").pattern("###").unlockedBy(hasItem, has(input)).save(con);
	}

	private void simple3x3(RecipeCategory category, ItemLike input, ItemLike output)
	{
		simple3x3(category, input, output, 1);
	}

	private void slabsStairs(RecipeCategory category, ItemLike block, ItemLike slab, ItemLike stair)
	{
		slabs(category, block, slab).save(con);
		stairs(category, block, stair).save(con);
	}

	private void slabsStairsWalls(RecipeCategory category, ItemLike block, ItemLike slab, ItemLike stair, ItemLike wall)
	{
		slabsStairs(category, block, slab, stair);
		walls(category, block, wall);
	}

	private void slabsStairs(RecipeCategory category, ItemLike block, ItemLike slab, ItemLike stair, boolean withStoneCutting)
	{
		slabsStairs(category, block, slab, stair);
		stoneCutting(category, block, List.of(slab, stair));
	}

	private void slabsStairsWalls(RecipeCategory category, ItemLike block, ItemLike slab, ItemLike stair, ItemLike wall, boolean withStoneCutting)
	{
		slabsStairsWalls(category, block, slab, stair, wall);
		stoneCutting(category, block, List.of(slab, stair, wall));
	}

	private ShapedRecipeBuilder slabs(RecipeCategory category, ItemLike ingredient, ItemLike slab)
	{
		return ShapedRecipeBuilder.shaped(category, slab, 6).define('#', ingredient).pattern("###").unlockedBy(hasItem, has(ingredient));
	}

	private ShapedRecipeBuilder stairs(RecipeCategory category, ItemLike ingredient, ItemLike stair)
	{
		return ShapedRecipeBuilder.shaped(category, stair, 4).define('#', ingredient).pattern("#  ").pattern("## ").pattern("###").unlockedBy(hasItem, has(ingredient));
	}

	private void walls(RecipeCategory category, ItemLike ingredient, ItemLike wall)
	{
		walls(category, ingredient, wall, null);
	}

	private void walls(RecipeCategory category, ItemLike ingredient, ItemLike wall, @Nullable String suffix)
	{
		ResourceLocation itemName = BuiltInRegistries.ITEM.getKey(wall.asItem());
		ResourceLocation name = new ResourceLocation(itemName.getNamespace(), itemName.getPath() + (suffix != null ? "_" + suffix : ""));
		ShapedRecipeBuilder.shaped(category, wall, 6).define('#', ingredient).pattern("###").pattern("###").unlockedBy(hasItem, has(ingredient)).save(con, name);
	}

	private void stoneCutting(RecipeCategory category, ItemLike ingredient, List<ItemLike> results)
	{
		results.forEach(result ->
		{
			SingleItemRecipeBuilder.stonecutting(Ingredient.of(ingredient), category, result, result instanceof SlabBlock ? 2 : 1).unlockedBy(hasItem, has(ingredient)).save(con, MoolandsMod.find(BuiltInRegistries.ITEM.getKey(result.asItem()).getPath() + "_stonecutting_" + BuiltInRegistries.ITEM.getKey(ingredient.asItem()).getPath()));
		});
	}

	/*@Override
	public String getName()
	{
		return MoolandsMod.MODID + " Recipes";
	}*/
}