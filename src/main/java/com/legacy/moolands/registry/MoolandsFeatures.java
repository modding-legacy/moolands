package com.legacy.moolands.registry;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import org.apache.commons.lang3.tuple.Pair;

import com.legacy.moolands.MoolandsMod;
import com.legacy.moolands.world.feature.BigBlueMushroomFeature;
import com.legacy.moolands.world.feature.MoolandLakesFeature;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.Registrar.Pointer;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.util.random.SimpleWeightedRandomList;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.HugeMushroomBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.blockpredicates.BlockPredicate;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.BlockStateConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.HugeMushroomFeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.OreConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.RandomPatchConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.SimpleBlockConfiguration;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.feature.stateproviders.WeightedStateProvider;
import net.minecraft.world.level.levelgen.placement.BiomeFilter;
import net.minecraft.world.level.levelgen.placement.CountPlacement;
import net.minecraft.world.level.levelgen.placement.HeightRangePlacement;
import net.minecraft.world.level.levelgen.placement.InSquarePlacement;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.placement.PlacementModifier;
import net.minecraft.world.level.levelgen.placement.RarityFilter;
import net.minecraft.world.level.levelgen.structure.templatesystem.BlockMatchTest;
import net.neoforged.neoforge.registries.RegisterEvent;

public class MoolandsFeatures
{
	public static final Feature<HugeMushroomFeatureConfiguration> BLUE_MUSHROOM = new BigBlueMushroomFeature<>(HugeMushroomFeatureConfiguration.CODEC);
	public static final Feature<BlockStateConfiguration> MOOLAND_LAKE = new MoolandLakesFeature(BlockStateConfiguration.CODEC);

	public static RegisterEvent registerEvent;

	public static void init(RegisterEvent event)
	{
		registerEvent = event;

		registerFeature("blue_mushroom", BLUE_MUSHROOM);
		registerFeature("mooland_lake", MOOLAND_LAKE);
	}

	private static void registerFeature(String key, Feature<?> feature)
	{
		registerEvent.register(Registries.FEATURE, MoolandsMod.locate(key), () -> feature);
	}

	public static class Configured
	{
		public static final RegistrarHandler<ConfiguredFeature<?, ?>> HANDLER = RegistrarHandler.getOrCreate(Registries.CONFIGURED_FEATURE, MoolandsMod.MODID);

		public static final Supplier<RandomPatchConfiguration> MOOLAND_GRASS_CONFIG = () -> simplePatch("moo_grass", MoolandsBlocks.mooland_grass.defaultBlockState(), 32);
		public static final Supplier<RandomPatchConfiguration> SMALL_MUSHROOM_CONFIG = () -> weightedPatch("small_mushrooms", SimpleWeightedRandomList.<BlockState>builder().add(MoolandsBlocks.blue_mushroom.defaultBlockState(), 3).add(MoolandsBlocks.yellow_mushroom.defaultBlockState(), 1), 32, (c) -> PlacementUtils.filtered(c.getLeft(), c.getRight(), BlockPredicate.allOf(BlockPredicate.matchesBlocks(BlockPos.ZERO, Blocks.AIR))));

		public static final Registrar.Pointer<ConfiguredFeature<?, ?>> MOOLAND_GRASS_PATCH = register("mooland_grass_patch", Feature.RANDOM_PATCH, MOOLAND_GRASS_CONFIG);
		public static final Registrar.Pointer<ConfiguredFeature<?, ?>> SMALL_MUSHROOMS = register("small_mushroom_patch", Feature.FLOWER, SMALL_MUSHROOM_CONFIG);

		public static final Registrar.Pointer<ConfiguredFeature<?, ?>> BLUE_MUSHROOM = register("blue_mushroom", MoolandsFeatures.BLUE_MUSHROOM, () -> new HugeMushroomFeatureConfiguration(BlockStateProvider.simple(MoolandsBlocks.blue_mushroom_block.defaultBlockState()), BlockStateProvider.simple(Blocks.MUSHROOM_STEM.defaultBlockState()), 2));
		public static final Registrar.Pointer<ConfiguredFeature<?, ?>> YELLOW_MUSHROOM = register("yellow_mushroom", Feature.HUGE_BROWN_MUSHROOM, () -> new HugeMushroomFeatureConfiguration(BlockStateProvider.simple(MoolandsBlocks.yellow_mushroom_block.defaultBlockState().setValue(HugeMushroomBlock.UP, true).setValue(HugeMushroomBlock.DOWN, false)), BlockStateProvider.simple(Blocks.MUSHROOM_STEM.defaultBlockState().setValue(HugeMushroomBlock.UP, false).setValue(HugeMushroomBlock.DOWN, false)), 3));

		public static final Registrar.Pointer<ConfiguredFeature<?, ?>> IRON_ORE = register("iron_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(new BlockMatchTest(MoolandsBlocks.moo_rock), MoolandsBlocks.moo_rock_iron_ore.defaultBlockState())), 10));

		public static final Registrar.Pointer<ConfiguredFeature<?, ?>> MOOLAND_LAKE = register("mooland_lake", MoolandsFeatures.MOOLAND_LAKE, () -> new BlockStateConfiguration(Blocks.WATER.defaultBlockState()));

		public static void init()
		{
		}

		private static <FC extends FeatureConfiguration, F extends Feature<FC>> Registrar.Pointer<ConfiguredFeature<?, ?>> register(String key, F feature, Supplier<FC> config)
		{
			return HANDLER.createPointer(key, () -> new ConfiguredFeature<>(feature, config.get()));
		}

		// PlacementUtils
		public static RandomPatchConfiguration simplePatch(String name, BlockState state, int tries)
		{
			return simplePatch(name, state, tries, p -> PlacementUtils.onlyWhenEmpty(p.getLeft(), p.getRight()));
		}

		public static RandomPatchConfiguration filteredPatch(String name, BlockState state, int tries, Block notBlock)
		{
			return simplePatch(name, state, tries, p -> PlacementUtils.filtered(p.getLeft(), p.getRight(), BlockPredicate.allOf(BlockPredicate.matchesBlocks(Blocks.AIR), BlockPredicate.not(BlockPredicate.matchesBlocks(BlockPos.ZERO.below(), notBlock)))));
		}

		@SuppressWarnings("unchecked")
		public static <FC extends FeatureConfiguration, F extends Feature<FC>> RandomPatchConfiguration simplePatch(String name, BlockState state, int tries, Function<Pair<F, FC>, Holder<PlacedFeature>> cons)
		{
			return randomPatchConfig(name, tries, cons.apply(Pair.of((F) Feature.SIMPLE_BLOCK, (FC) new SimpleBlockConfiguration(BlockStateProvider.simple(state)))));
		}

		public static RandomPatchConfiguration weightedPatch(String name, SimpleWeightedRandomList.Builder<BlockState> builder, int tries)
		{
			return weightedPatch(name, builder, tries, p -> PlacementUtils.onlyWhenEmpty(p.getLeft(), p.getRight()));
		}

		@SuppressWarnings("unchecked")
		public static <FC extends FeatureConfiguration, F extends Feature<FC>> RandomPatchConfiguration weightedPatch(String name, SimpleWeightedRandomList.Builder<BlockState> builder, int tries, Function<Pair<F, FC>, Holder<PlacedFeature>> cons)
		{
			return randomPatchConfig(name, tries, cons.apply(Pair.of((F) Feature.SIMPLE_BLOCK, (FC) new SimpleBlockConfiguration(new WeightedStateProvider(builder)))));
		}

		public static <FC extends FeatureConfiguration, F extends Feature<FC>> RandomPatchConfiguration randomPatchConfig(String name, int tries, Holder<PlacedFeature> placedHolder)
		{
			return FeatureUtils.simpleRandomPatchConfiguration(tries, placedHolder);
		}
	}

	public static class Placements
	{
		public static final RegistrarHandler<PlacedFeature> HANDLER = RegistrarHandler.getOrCreate(Registries.PLACED_FEATURE, MoolandsMod.MODID);

		public static final Registrar.Pointer<PlacedFeature> MOOLAND_GRASS_PATCH_180 = register("mooland_grass_180", Configured.MOOLAND_GRASS_PATCH, countRange(201, 180));
		public static final Registrar.Pointer<PlacedFeature> SMALL_MUSHROOM_PATCH_32 = register("small_mushrooms_32", Configured.SMALL_MUSHROOMS, countRange(201, 20));

		public static final Registrar.Pointer<PlacedFeature> HUGE_BLUE_MUSHROOM = register("huge_blue_mushroom", Configured.BLUE_MUSHROOM, countRange(202, 60));
		public static final Registrar.Pointer<PlacedFeature> HUGE_YELLOW_MUSHROOM = register("huge_yellow_mushroom", Configured.YELLOW_MUSHROOM, countRange(35, 10));

		public static final Registrar.Pointer<PlacedFeature> IRON_ORE = register("iron_ore", Configured.IRON_ORE, commonOrePlacement(30, HeightRangePlacement.uniform(VerticalAnchor.BOTTOM, VerticalAnchor.TOP)));

		public static final Registrar.Pointer<PlacedFeature> MOOLAND_LAKE = register("mooland_lake", Configured.MOOLAND_LAKE, List.of(RarityFilter.onAverageOnceEvery(10), InSquarePlacement.spread(), PlacementUtils.FULL_RANGE, BiomeFilter.biome()));

		public static void init()
		{
		}

		private static Pointer<PlacedFeature> register(String key, Registrar.Pointer<ConfiguredFeature<?, ?>> feature, List<PlacementModifier> mods)
		{
			return HANDLER.createPointer(key, (b) -> new PlacedFeature(feature.getHolder(b).get(), List.copyOf(mods)));
		}

		private static List<PlacementModifier> countRange(int height, int count)
		{
			return List.of(CountPlacement.of(count), InSquarePlacement.spread(), HeightRangePlacement.uniform(VerticalAnchor.absolute(0), VerticalAnchor.absolute(height)), BiomeFilter.biome());
		}

		private static List<PlacementModifier> commonOrePlacement(int pCount, PlacementModifier pHeightRange)
		{
			return orePlacement(CountPlacement.of(pCount), pHeightRange);
		}

		private static List<PlacementModifier> orePlacement(PlacementModifier p_195347_, PlacementModifier p_195348_)
		{
			return List.of(p_195347_, InSquarePlacement.spread(), p_195348_, BiomeFilter.biome());
		}
	}
}
