package com.legacy.moolands.registry;

import java.util.function.Function;

import com.legacy.moolands.MoolandsMod;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.level.biome.AmbientMoodSettings;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeGenerationSettings;
import net.minecraft.world.level.biome.BiomeSpecialEffects;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.minecraft.world.level.levelgen.GenerationStep;

public class MoolandsBiomes
{
	public static final RegistrarHandler<Biome> HANDLER = RegistrarHandler.getOrCreate(Registries.BIOME, MoolandsMod.MODID);

	public static final Registrar.Pointer<Biome> AWKWARD_HEIGHTS = create("awkward_heights", Builders::createAwkwardHeightsBiome);

	private static Registrar.Pointer<Biome> create(String name, Function<BootstapContext<?>, Biome> biome)
	{
		return HANDLER.createPointer(name, biome);
	}

	public static class Builders
	{
		public static Biome createAwkwardHeightsBiome(BootstapContext<?> bootstrap)
		{
			MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();

			spawns.addSpawn(MobCategory.CREATURE, new MobSpawnSettings.SpawnerData(MoolandsEntityTypes.AWFUL_COW, 70, 3, 5));

			BiomeGenerationSettings.Builder builder = new BiomeGenerationSettings.Builder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));

			builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, MoolandsFeatures.Placements.HUGE_BLUE_MUSHROOM.getKey());
			builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, MoolandsFeatures.Placements.HUGE_YELLOW_MUSHROOM.getKey());

			builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, MoolandsFeatures.Placements.MOOLAND_GRASS_PATCH_180.getKey());
			builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, MoolandsFeatures.Placements.SMALL_MUSHROOM_PATCH_32.getKey());

			builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, MoolandsFeatures.Placements.IRON_ORE.getKey());

			builder.addFeature(GenerationStep.Decoration.LOCAL_MODIFICATIONS, MoolandsFeatures.Placements.MOOLAND_LAKE.getKey());

			boolean useOld = true;
			// 2b80ff
			return (new Biome.BiomeBuilder()).hasPrecipitation(false).temperature(0.5F).downfall(0.4F).specialEffects((new BiomeSpecialEffects.Builder()).grassColorOverride(0xff744a).foliageColorOverride(0xff744a).skyColor(useOld ? 12632319 : 0x2b80ff).waterColor(0xffffff).waterFogColor(0xffffff).fogColor(useOld ? 8421536 : 12638463).ambientMoodSound(AmbientMoodSettings.LEGACY_CAVE_SETTINGS).build()).mobSpawnSettings(spawns.build()).generationSettings(builder.build()).build();
		}
	}
}
