package com.legacy.moolands.registry;

import com.legacy.moolands.MoolandsMod;
import com.legacy.moolands.world.structure.MushroomCampPieces;
import com.legacy.moolands.world.structure.MushroomCampStructure;
import com.legacy.moolands.world.structure.MushroomHousePieces;
import com.legacy.moolands.world.structure.MushroomHouseStructure;
import com.legacy.structure_gel.api.registry.registrar.StructureRegistrar;
import com.legacy.structure_gel.api.structure.GridStructurePlacement;

import net.minecraft.world.level.levelgen.structure.StructureSpawnOverride.BoundingBoxType;

public class MoolandsStructures
{
	//@formatter:off
	public static final StructureRegistrar<MushroomHouseStructure> MUSHROOM_HOUSE = StructureRegistrar.builder(MoolandsMod.locate("mushroom_house"), () -> () -> MushroomHouseStructure.CODEC)
			.addPiece("main", () -> MushroomHousePieces.Piece::new)
			.pushStructure(MushroomHouseStructure::new)
				.biomes(MoolandsBiomes.AWKWARD_HEIGHTS.getKey())
				.noSpawns(BoundingBoxType.PIECE)
			.popStructure()
			.placement(() -> GridStructurePlacement.builder(16, 6, 0.5F).build(MoolandsStructures.MUSHROOM_HOUSE.getRegistryName()))
			.build();
	
	public static final StructureRegistrar<MushroomCampStructure> MUSHROOM_CAMP = StructureRegistrar.builder(MoolandsMod.locate("mushroom_camp"), () -> () -> MushroomCampStructure.CODEC)
			.addPiece("main", () -> MushroomCampPieces.Piece::new)
			.pushStructure(MushroomCampStructure::new)
				.biomes(MoolandsBiomes.AWKWARD_HEIGHTS.getKey())
				.noSpawns(BoundingBoxType.PIECE)
			.popStructure()
			.placement(() -> GridStructurePlacement.builder(12, 5, 0.7F).build(MoolandsStructures.MUSHROOM_CAMP.getRegistryName()))
			.build();

	//@formatter:on
	public static void init()
	{
	}
}
