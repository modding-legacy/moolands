package com.legacy.moolands.data;

import java.util.Optional;
import java.util.Set;

import com.legacy.moolands.MoolandsMod;
import com.legacy.structure_gel.api.data.providers.NestedDataProvider;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.DetectedVersion;
import net.minecraft.core.RegistrySetBuilder;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.data.metadata.PackMetadataGenerator;
import net.minecraft.network.chat.Component;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.metadata.pack.PackMetadataSection;
import net.minecraft.util.InclusiveRange;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.Mod.EventBusSubscriber;
import net.neoforged.fml.common.Mod.EventBusSubscriber.Bus;
import net.neoforged.neoforge.common.data.BlockTagsProvider;
import net.neoforged.neoforge.common.data.DatapackBuiltinEntriesProvider;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import net.neoforged.neoforge.data.event.GatherDataEvent;

@EventBusSubscriber(modid = MoolandsMod.MODID, bus = Bus.MOD)
public class MLDataGen
{
	@SubscribeEvent
	public static void gatherData(GatherDataEvent event)
	{
		DataGenerator gen = event.getGenerator();
		ExistingFileHelper helper = event.getExistingFileHelper();
		PackOutput output = gen.getPackOutput();

		/*MoolandsFeatures.Configured.init();
		MoolandsFeatures.Placements.init();*/

		boolean server = event.includeServer();
		DatapackBuiltinEntriesProvider provider = new DatapackBuiltinEntriesProvider(gen.getPackOutput(), event.getLookupProvider(), RegistrarHandler.injectRegistries(new RegistrySetBuilder()), Set.of(MoolandsMod.MODID));;
		var lookup = provider.getRegistryProvider();
		gen.addProvider(server, provider);

		BlockTagsProvider blockTagProv = new MLTagProv.BlockTagProv(gen, helper, lookup);
		gen.addProvider(server, blockTagProv);
		gen.addProvider(server, new MLTagProv.ItemTagProv(gen, blockTagProv.contentsGetter(), helper, lookup));

		gen.addProvider(server, new MLRecipeProv(gen.getPackOutput(), lookup));
		gen.addProvider(server, new MLLootProv(gen));
	
		gen.addProvider(server, packMcmeta(output, "Moolands' resources"));

		PackOutput legacyPackOutput = gen.getPackOutput("assets/moolands/legacy_pack");
		gen.addProvider(server, packMcmeta(legacyPackOutput, "Moolands' old textures from pre-1.14"));
	}

	private static final DataProvider packMcmeta(PackOutput output, String description)
	{
		int serverVersion = DetectedVersion.BUILT_IN.getPackVersion(PackType.SERVER_DATA);
		return NestedDataProvider.of(new PackMetadataGenerator(output).add(PackMetadataSection.TYPE, new PackMetadataSection(Component.literal(description), serverVersion, Optional.of(new InclusiveRange<>(0, Integer.MAX_VALUE)))), description);
	}
}
