package com.legacy.moolands.client;

import com.legacy.moolands.registry.MoolandsDimensions;

import net.minecraft.client.renderer.DimensionSpecialEffects;
import net.minecraft.world.phys.Vec3;

public class MoolandsClientEvents
{
	private static final MoolandRenderInfo MOOLANDS_RENDER_INFO = new MoolandRenderInfo();

	public static void initDimensionRenderInfo()
	{
		DimensionSpecialEffects.EFFECTS.put(MoolandsDimensions.MOOLANDS_ID, MOOLANDS_RENDER_INFO);
	}

	private static class MoolandRenderInfo extends DimensionSpecialEffects
	{
		public MoolandRenderInfo()
		{
			super(320.0F, false, DimensionSpecialEffects.SkyType.NORMAL, false, false);
		}

		@Override
		public Vec3 getBrightnessDependentFogColor(Vec3 pFogColor, float pBrightness)
		{
			return pFogColor.multiply((double) (pBrightness * 0.94F + 0.06F), (double) (pBrightness * 0.94F + 0.06F), (double) (pBrightness * 0.91F + 0.09F));
		}

		@Override
		public boolean isFoggyAt(int pX, int pY)
		{
			return false;
		}
	}
}
