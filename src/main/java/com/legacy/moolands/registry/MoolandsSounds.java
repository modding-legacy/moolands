package com.legacy.moolands.registry;

import com.legacy.moolands.MoolandsMod;

import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;

public class MoolandsSounds
{
	public static final SoundEvent AWFUL_COW_AMBIENT = create("cow_ambient");
	public static final SoundEvent AWFUL_COW_HURT = create("cow_hurt");
	public static final SoundEvent AWFUL_COW_DEATH = create("cow_death");

	public static final SoundEvent MOO_DISC = create("moo_disc");

	public static void init()
	{
	}

	private static SoundEvent create(String name)
	{
		ResourceLocation location = MoolandsMod.locate(name);
		SoundEvent sound = SoundEvent.createVariableRangeEvent(location);
		Registry.register(BuiltInRegistries.SOUND_EVENT, location, sound);

		return sound;
	}
}