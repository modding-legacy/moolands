package com.legacy.moolands.client.resource_pack;

import com.legacy.moolands.MoolandsMod;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.PathPackResources;
import net.minecraft.server.packs.repository.BuiltInPackSource;
import net.minecraft.server.packs.repository.Pack;
import net.minecraft.server.packs.repository.PackSource;
import net.neoforged.fml.ModList;
import net.neoforged.neoforge.event.AddPackFindersEvent;
import net.neoforged.neoforgespi.locating.IModFile;

public class MLResourcePackHandler
{
	public static final Pack LEGACY_PACK = createPack(Component.literal("Moolands Programmer Art"), "legacy_pack").hidden();

	public static void packRegistry(AddPackFindersEvent event)
	{
		if (event.getPackType() == PackType.CLIENT_RESOURCES)
		{
		}
	}

	@SuppressWarnings("unused")
	private static void register(AddPackFindersEvent event, MutableComponent name, String folder)
	{
		event.addRepositorySource((consumer) -> consumer.accept(createPack(name, folder)));
	}

	public static Pack createPack(MutableComponent name, String folder)
	{
		IModFile file = ModList.get().getModFileById(MoolandsMod.MODID).getFile();
		PathPackResources packResources = new PathPackResources(MoolandsMod.find(folder), file.findResource("assets/" + MoolandsMod.MODID + "/" + folder), true);
		return Pack.readMetaAndCreate(MoolandsMod.find(folder), name, false, BuiltInPackSource.fixedResources(packResources), PackType.CLIENT_RESOURCES, Pack.Position.TOP, PackSource.BUILT_IN);
	}
}
