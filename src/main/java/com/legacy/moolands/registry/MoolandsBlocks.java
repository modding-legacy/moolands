package com.legacy.moolands.registry;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.legacy.moolands.MoolandsMod;
import com.legacy.moolands.block.MoolandGrassBlock;
import com.legacy.moolands.block.MoolandMushroomBlock;
import com.legacy.moolands.block.MoolandPortalBlock;
import com.legacy.moolands.block.MoolandTallGrassBlock;
import com.legacy.moolands.block.MoolandsFlowerPotBlock;

import net.minecraft.core.registries.Registries;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.DropExperienceBlock;
import net.minecraft.world.level.block.HugeMushroomBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.WallBlock;
import net.neoforged.neoforge.registries.RegisterEvent;

public class MoolandsBlocks
{
	public static MoolandPortalBlock mooland_portal;

	public static Block mooland_grass, mooland_grass_block, mooland_dirt;

	public static Block moo_rock, moo_rock_slab, moo_rock_stairs, moo_rock_wall;

	public static Block moo_rock_bricks, moo_rock_brick_slab, moo_rock_brick_stairs, moo_rock_brick_wall;

	public static Block moo_rock_iron_ore;

	public static Block blue_mushroom_block, yellow_mushroom_block, blue_mushroom, yellow_mushroom;

	public static Block potted_blue_mushroom, potted_yellow_mushroom, potted_mooland_grass;

	public static List<Block> blockList = new ArrayList<>();
	public static Map<Block, Item.Properties> blockItemPropertiesMap = new LinkedHashMap<>();

	private static RegisterEvent registry;

	public static void init(RegisterEvent event)
	{
		MoolandsBlocks.registry = event;

		mooland_portal = registerBlock("mooland_portal", new MoolandPortalBlock(Block.Properties.ofFullCopy(Blocks.NETHER_PORTAL).noLootTable()));

		mooland_grass = register("mooland_grass", new MoolandTallGrassBlock(Block.Properties.ofFullCopy(Blocks.SHORT_GRASS)));
		mooland_grass_block = register("mooland_grass_block", new MoolandGrassBlock(Block.Properties.ofFullCopy(Blocks.GRASS_BLOCK)));
		mooland_dirt = register("mooland_dirt", new Block(Block.Properties.ofFullCopy(Blocks.DIRT)));
		moo_rock = register("moo_rock", new Block(Block.Properties.ofFullCopy(Blocks.STONE)));

		moo_rock_slab = register("moo_rock_slab", new SlabBlock(Block.Properties.ofFullCopy(Blocks.COBBLESTONE_SLAB)));
		moo_rock_stairs = register("moo_rock_stairs", new StairBlock(() -> moo_rock.defaultBlockState(), Block.Properties.ofFullCopy(moo_rock)));
		moo_rock_wall = register("moo_rock_wall", new WallBlock(Block.Properties.ofFullCopy(moo_rock)));

		moo_rock_bricks = register("moo_rock_bricks", new Block(Block.Properties.ofFullCopy(Blocks.STONE_BRICKS)));
		moo_rock_brick_slab = register("moo_rock_brick_slab", new SlabBlock(Block.Properties.ofFullCopy(Blocks.STONE_BRICK_SLAB)));
		moo_rock_brick_stairs = register("moo_rock_brick_stairs", new StairBlock(() -> moo_rock_bricks.defaultBlockState(), Block.Properties.ofFullCopy(moo_rock_bricks)));
		moo_rock_brick_wall = register("moo_rock_brick_wall", new WallBlock(Block.Properties.ofFullCopy(moo_rock_bricks)));

		moo_rock_iron_ore = register("moo_rock_iron_ore", new DropExperienceBlock(UniformInt.of(0, 1), Block.Properties.ofFullCopy(Blocks.IRON_ORE)));

		blue_mushroom_block = register("blue_mushroom_block", new HugeMushroomBlock(Block.Properties.ofFullCopy(Blocks.RED_MUSHROOM_BLOCK).sound(SoundType.FUNGUS)));
		yellow_mushroom_block = register("yellow_mushroom_block", new HugeMushroomBlock(Block.Properties.ofFullCopy(Blocks.BROWN_MUSHROOM_BLOCK).sound(SoundType.FUNGUS)));
		blue_mushroom = register("blue_mushroom", new MoolandMushroomBlock(Block.Properties.ofFullCopy(Blocks.RED_MUSHROOM).sound(SoundType.FUNGUS), MoolandsFeatures.Configured.BLUE_MUSHROOM.getKey()));
		yellow_mushroom = register("yellow_mushroom", new MoolandMushroomBlock(Block.Properties.ofFullCopy(Blocks.BROWN_MUSHROOM).sound(SoundType.FUNGUS), MoolandsFeatures.Configured.YELLOW_MUSHROOM.getKey()));

		potted_blue_mushroom = registerBlock("potted_blue_mushroom", new MoolandsFlowerPotBlock(() -> blue_mushroom));
		potted_yellow_mushroom = registerBlock("potted_yellow_mushroom", new MoolandsFlowerPotBlock(() -> yellow_mushroom));
		potted_mooland_grass = registerBlock("potted_mooland_grass", new MoolandsFlowerPotBlock(() -> mooland_grass));
	}

	public static <T extends CreativeModeTab, B extends Block> B register(String key, B block)
	{
		blockList.add(block);
		return registerBlock(key, block);
	}

	public static <B extends Block> B registerBlock(String key, B block)
	{
		if (registry != null)
			registry.register(Registries.BLOCK, MoolandsMod.locate(key), () -> block);

		return block;
	}
}