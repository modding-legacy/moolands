package com.legacy.moolands.item;

import com.legacy.moolands.MoolandsMod;
import com.legacy.moolands.registry.MoolandsBlocks;
import com.legacy.moolands.registry.MoolandsRegistry;

import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.CreativeModeTabs;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.Mod;
import net.neoforged.neoforge.event.BuildCreativeModeTabContentsEvent;
import net.neoforged.neoforge.registries.RegisterEvent;

@Mod.EventBusSubscriber(modid = MoolandsMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class MoolandsCreativeTabs
{
	@SubscribeEvent
	public static void modifyExisting(BuildCreativeModeTabContentsEvent event)
	{
		if (event.getTabKey() == CreativeModeTabs.SPAWN_EGGS)
			event.accept(MoolandsRegistry.AWFUL_COW_SPAWN_EGG.get());
	}

	@SubscribeEvent
	public static void register(RegisterEvent event)
	{
		// @formatter:off
		event.register(Registries.CREATIVE_MODE_TAB, MoolandsMod.locate("all_items"), () -> CreativeModeTab.builder().icon(() -> MoolandsBlocks.mooland_grass_block.asItem().getDefaultInstance()).title(Component.translatable("itemGroup.moolands.all"))
				.displayItems((flags, output) ->
				{
					output.accept(MoolandsRegistry.MOO_DISC.get());
					output.accept(MoolandsRegistry.AWFUL_COW_SPAWN_EGG.get());

					output.accept(MoolandsBlocks.mooland_grass);
					output.accept(MoolandsBlocks.mooland_grass_block);
					output.accept(MoolandsBlocks.mooland_dirt);

					output.accept(MoolandsBlocks.blue_mushroom);
					output.accept(MoolandsBlocks.blue_mushroom_block);
					output.accept(MoolandsBlocks.yellow_mushroom);
					output.accept(MoolandsBlocks.yellow_mushroom_block);

					output.accept(MoolandsBlocks.moo_rock);
					output.accept(MoolandsBlocks.moo_rock_stairs);
					output.accept(MoolandsBlocks.moo_rock_slab);
					output.accept(MoolandsBlocks.moo_rock_wall);
					
					output.accept(MoolandsBlocks.moo_rock_bricks);
					output.accept(MoolandsBlocks.moo_rock_brick_stairs);
					output.accept(MoolandsBlocks.moo_rock_brick_slab);
					output.accept(MoolandsBlocks.moo_rock_brick_wall);
					
					output.accept(MoolandsBlocks.moo_rock_iron_ore);
				}).build());
		// @formatter:on
	}
}
