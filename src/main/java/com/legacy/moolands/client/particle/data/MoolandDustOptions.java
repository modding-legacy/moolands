package com.legacy.moolands.client.particle.data;

import org.joml.Vector3f;

import com.legacy.moolands.registry.MoolandsParticles;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.particles.DustParticleOptionsBase;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.util.ExtraCodecs;

public class MoolandDustOptions extends DustParticleOptionsBase
{
	public static final MoolandDustOptions MILKY_PORTAL = new MoolandDustOptions(new Vector3f(1.0F, 1.0F, 1.0F), 1.4F, true);
	public static final MoolandDustOptions YELLOW_COW = new MoolandDustOptions(new Vector3f(0.976F, 0.745F, 0.0F), 1.4F, false);
	public static final MoolandDustOptions JONATHING_COW = new MoolandDustOptions(new Vector3f(0.976F, 0.645F, 0.976F), 1.0F, false);

	
	public static final Codec<MoolandDustOptions> CODEC = RecordCodecBuilder.create((instance) ->
	{
		return instance.group(ExtraCodecs.VECTOR3F.fieldOf("color").forGetter((particle) ->
		{
			return particle.color;
		}), Codec.FLOAT.fieldOf("scale").forGetter((particle) ->
		{
			return particle.scale;
		}), Codec.BOOL.fieldOf("blue_tint_only").forGetter((particle) ->
		{
			return particle.blueTintOnly;
		})).apply(instance, MoolandDustOptions::new);
	});

	@SuppressWarnings("deprecation")
	public static final ParticleOptions.Deserializer<MoolandDustOptions> DESERIALIZER = new ParticleOptions.Deserializer<MoolandDustOptions>()
	{
		@Override
		public MoolandDustOptions fromCommand(ParticleType<MoolandDustOptions> particle, StringReader data) throws CommandSyntaxException
		{
			Vector3f vector3f = DustParticleOptionsBase.readVector3f(data);
			data.expect(' ');
			float f = data.readFloat();
			return new MoolandDustOptions(vector3f, f, false);
		}

		@Override
		public MoolandDustOptions fromNetwork(ParticleType<MoolandDustOptions> particle, FriendlyByteBuf buff)
		{
			return new MoolandDustOptions(DustParticleOptionsBase.readVector3f(buff), buff.readFloat(), buff.readBoolean());
		}
	};

	private final boolean blueTintOnly;

	public MoolandDustOptions(Vector3f colorVec, float size, boolean blueTintOnly)
	{
		super(colorVec, size);

		this.blueTintOnly = blueTintOnly;
	}

	@Override
	public ParticleType<MoolandDustOptions> getType()
	{
		return MoolandsParticles.MOOLAND_DUST;
	}

	@Override
	public void writeToNetwork(FriendlyByteBuf pBuffer)
	{
		super.writeToNetwork(pBuffer);
		pBuffer.writeBoolean(this.blueTintOnly);
	}

	public boolean isBlueTintOnly()
	{
		return this.blueTintOnly;
	}
}