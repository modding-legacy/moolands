package com.legacy.moolands.data;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

import com.legacy.moolands.MoolandsMod;
import com.legacy.moolands.registry.MoolandsBlocks;
import com.legacy.moolands.registry.MoolandsEntityTypes;
import com.legacy.moolands.registry.MoolandsRegistry;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.data.loot.LootTableSubProvider;
import net.minecraft.data.loot.packs.VanillaBlockLoot;
import net.minecraft.data.loot.packs.VanillaEntityLoot;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.storage.loot.BuiltInLootTables;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.LootDataId;
import net.minecraft.world.level.storage.loot.LootDataType;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.ValidationContext;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.entries.LootPoolEntryContainer;
import net.minecraft.world.level.storage.loot.functions.ApplyBonusCount;
import net.minecraft.world.level.storage.loot.functions.LootingEnchantFunction;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.functions.SmeltItemFunction;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraft.world.level.storage.loot.predicates.LootItemEntityPropertyCondition;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;

public class MLLootProv extends LootTableProvider
{
	public static final ResourceLocation CLASSIC_MUSHROOM_HOUSE_CHEST = MoolandsMod.locate("chests/mushroom_house/classic");
	public static final ResourceLocation CLASSIC_MUSHROOM_HOUSE_CHEST_LARGE = MoolandsMod.locate("chests/mushroom_house/classic_large");

	public static final ResourceLocation SMALL_MUSHROOM_HOUSE_CHEST = MoolandsMod.locate("chests/mushroom_house/small");

	public MLLootProv(DataGenerator dataGeneratorIn)
	{
		super(dataGeneratorIn.getPackOutput(), Set.of(), List.of(new LootTableProvider.SubProviderEntry(MLBlockLoot::new, LootContextParamSets.BLOCK), new LootTableProvider.SubProviderEntry(MLEntityLoot::new, LootContextParamSets.ENTITY), new LootTableProvider.SubProviderEntry(MLChestLoot::new, LootContextParamSets.CHEST)));
	}

	@Override
	protected void validate(Map<ResourceLocation, LootTable> map, ValidationContext context)
	{
		map.forEach((name, lootTable) ->
		{
			lootTable.validate(context.setParams(lootTable.getParamSet()).enterElement("{" + name + "}", new LootDataId<>(LootDataType.TABLE, name)));
		});
	}

	/*@Override
	public String getName()
	{
		return MoolandsMod.MODID + " Loot Tables";
	}*/

	private static class MLBlockLoot extends VanillaBlockLoot
	{
		@Override
		protected void generate()
		{
			blocks().forEach(block ->
			{
				if (block == MoolandsBlocks.mooland_grass)
					add(block, VanillaBlockLoot::createShearsOnlyDrop);
				else if (block == MoolandsBlocks.mooland_grass_block)
					add(block, (b) -> createSingleItemTableWithSilkTouch(b, MoolandsBlocks.mooland_dirt));
				else if (block == MoolandsBlocks.blue_mushroom_block)
					add(block, (b) -> createMushroomBlockDrop(b, MoolandsBlocks.blue_mushroom));
				else if (block == MoolandsBlocks.yellow_mushroom_block)
					add(block, (b) -> createMushroomBlockDrop(b, MoolandsBlocks.yellow_mushroom));
				else if (block == MoolandsBlocks.moo_rock_iron_ore)
					add(block, (b) -> createSilkTouchDispatchTable(b, this.applyExplosionDecay(b, LootItem.lootTableItem(Items.IRON_NUGGET).apply(SetItemCountFunction.setCount(UniformGenerator.between(4.0F, 6.0F))).apply(ApplyBonusCount.addOreBonusCount(Enchantments.BLOCK_FORTUNE)))));
				else if (block instanceof SlabBlock)
					add(block, this::createSlabItemTable);
				else if (block instanceof FlowerPotBlock)
					dropPottedContents(block);
				else
					dropSelf(block);
			});
		}

		@Override
		protected Iterable<Block> getKnownBlocks()
		{
			return blocks()::iterator;
		}

		private Stream<Block> blocks()
		{
			return BuiltInRegistries.BLOCK.stream().filter(b -> BuiltInRegistries.BLOCK.getKey(b).getNamespace().equals(MoolandsMod.MODID) && !b.getLootTable().equals(BuiltInLootTables.EMPTY));
		}
	}

	private static class MLEntityLoot extends VanillaEntityLoot
	{
		@Override
		public void generate()
		{
			// @formatter:off
		      this.add(MoolandsEntityTypes.AWFUL_COW, 
		    		  LootTable.lootTable()
		    		  .withPool(LootPool.lootPool().setRolls(ConstantValue.exactly(1.0F)).add(LootItem.lootTableItem(Items.LEATHER).apply(SetItemCountFunction.setCount(UniformGenerator.between(0.0F, 2.0F))).apply(LootingEnchantFunction.lootingMultiplier(UniformGenerator.between(0.0F, 1.0F)))))
		    		  .withPool(LootPool.lootPool().setRolls(ConstantValue.exactly(1.0F))
		    				  .add(meatPool(Items.BEEF))
		    				  .add(meatPool(Items.PORKCHOP))
		    				  .add(meatPool(Items.RABBIT))
		    				  .add(meatPool(Items.MUTTON))
		    				  .add(meatPool(Items.ROTTEN_FLESH))));
		   // @formatter:on
		}

		private LootPoolEntryContainer.Builder<?> meatPool(Item meat)
		{
			return LootItem.lootTableItem(meat).apply(SetItemCountFunction.setCount(UniformGenerator.between(1.0F, 3.0F))).apply(SmeltItemFunction.smelted().when(LootItemEntityPropertyCondition.hasProperties(LootContext.EntityTarget.THIS, ENTITY_ON_FIRE))).apply(LootingEnchantFunction.lootingMultiplier(UniformGenerator.between(0.0F, 1.0F)));
		}

		@Override
		protected Stream<EntityType<?>> getKnownEntityTypes()
		{
			return BuiltInRegistries.ENTITY_TYPE.stream().filter(e -> BuiltInRegistries.ENTITY_TYPE.getKey(e).getNamespace().contains(MoolandsMod.MODID));
		}
	}

	public static class MLChestLoot implements LootTableSubProvider
	{
		@Override
		public void generate(BiConsumer<ResourceLocation, LootTable.Builder> consumer)
		{
			// @formatter:off
			consumer.accept(CLASSIC_MUSHROOM_HOUSE_CHEST, LootTable.lootTable().withPool(
					poolOf(List.of(
							LootItem.lootTableItem(MoolandsRegistry.MOO_DISC.get()).setWeight(1),
							LootItem.lootTableItem(Items.NAME_TAG).setWeight(2),
							LootItem.lootTableItem(Items.IRON_NUGGET).apply(SetItemCountFunction.setCount(UniformGenerator.between(2, 3))).setWeight(4),
							LootItem.lootTableItem(MoolandsBlocks.yellow_mushroom).apply(SetItemCountFunction.setCount(UniformGenerator.between(3, 5))).setWeight(4),
							LootItem.lootTableItem(MoolandsBlocks.blue_mushroom).apply(SetItemCountFunction.setCount(UniformGenerator.between(5, 8))).setWeight(6),
							LootItem.lootTableItem(Items.GLOWSTONE_DUST).apply(SetItemCountFunction.setCount(UniformGenerator.between(4, 8))).setWeight(7)
						)).setRolls(UniformGenerator.between(4, 8))));
			
			consumer.accept(CLASSIC_MUSHROOM_HOUSE_CHEST_LARGE, LootTable.lootTable().withPool(
					poolOf(List.of(LootItem.lootTableItem(Items.SADDLE))))
					.withPool(poolOf(List.of(
							LootItem.lootTableItem(MoolandsRegistry.MOO_DISC.get()).setWeight(1),
							LootItem.lootTableItem(MoolandsBlocks.yellow_mushroom).apply(SetItemCountFunction.setCount(UniformGenerator.between(3, 5))).setWeight(2),
							LootItem.lootTableItem(MoolandsBlocks.blue_mushroom).apply(SetItemCountFunction.setCount(UniformGenerator.between(5, 8))).setWeight(3),
							LootItem.lootTableItem(Items.IRON_NUGGET).apply(SetItemCountFunction.setCount(UniformGenerator.between(3, 5))).setWeight(4),
							LootItem.lootTableItem(Items.GLOWSTONE_DUST).apply(SetItemCountFunction.setCount(UniformGenerator.between(2, 4))).setWeight(7)
						)).setRolls(UniformGenerator.between(4, 8))));
			
			consumer.accept(SMALL_MUSHROOM_HOUSE_CHEST, LootTable.lootTable().withPool(
					poolOf(List.of(
							LootItem.lootTableItem(Items.STICK).apply(SetItemCountFunction.setCount(UniformGenerator.between(3, 5))).setWeight(3),
							LootItem.lootTableItem(Items.GLOWSTONE_DUST).apply(SetItemCountFunction.setCount(UniformGenerator.between(2, 4))).setWeight(4),
							LootItem.lootTableItem(MoolandsBlocks.yellow_mushroom).apply(SetItemCountFunction.setCount(UniformGenerator.between(3, 5))).setWeight(4),
							LootItem.lootTableItem(Items.STRING).apply(SetItemCountFunction.setCount(UniformGenerator.between(4, 8))).setWeight(5),
							LootItem.lootTableItem(MoolandsBlocks.blue_mushroom).apply(SetItemCountFunction.setCount(UniformGenerator.between(5, 8))).setWeight(6)
						)).setRolls(UniformGenerator.between(4, 5))));
			// @formatter:on
		}

		private LootPool.Builder poolOf(List<LootPoolEntryContainer.Builder<?>> lootEntries)
		{
			LootPool.Builder pool = LootPool.lootPool();
			lootEntries.forEach(entry -> pool.add(entry));
			return pool;
		}
	}
}
