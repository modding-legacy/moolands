package com.legacy.moolands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.legacy.moolands.client.MoolandsClientEvents;
import com.legacy.moolands.client.render.MoolandEntityRendering;
import com.legacy.moolands.client.resource_pack.MLResourcePackHandler;
import com.legacy.moolands.registry.MoolandsBiomes;
import com.legacy.moolands.registry.MoolandsDimensions;
import com.legacy.moolands.registry.MoolandsEntityTypes;
import com.legacy.moolands.registry.MoolandsFeatures;
import com.legacy.moolands.registry.MoolandsPoiTypes;
import com.legacy.moolands.registry.MoolandsRegistry;
import com.legacy.moolands.registry.MoolandsStructures;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.EventPriority;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.event.lifecycle.FMLClientSetupEvent;
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.common.NeoForge;

@Mod(MoolandsMod.MODID)
public class MoolandsMod
{
	public static final String MODID = "moolands";
	public static final Logger LOGGER = LogManager.getLogger("ModdingLegacy/" + MODID);

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MoolandsMod.MODID, name);
	}

	public static String find(String name)
	{
		return MoolandsMod.MODID + ":" + name;
	}

	public MoolandsMod(IEventBus modBus)
	{
		IEventBus forgeBus = NeoForge.EVENT_BUS;

		if (FMLEnvironment.dist == Dist.CLIENT)
		{
			MoolandEntityRendering.init(modBus);

			modBus.addListener(com.legacy.moolands.registry.MoolandsParticles.Register::registerParticleFactories);
			modBus.addListener(MLResourcePackHandler::packRegistry);
			modBus.addListener(MoolandsMod::clientInit);
			/*forgeBus.register(MoolandsClientEvents.class);*/
		}

		MoolandsStructures.init();
		MoolandsDimensions.init();

		RegistrarHandler.registerHandlers(MODID, modBus, MoolandsFeatures.Configured.HANDLER, MoolandsFeatures.Placements.HANDLER, MoolandsBiomes.HANDLER, MoolandsPoiTypes.HANDLER);

		modBus.register(MoolandsRegistry.class);
		modBus.addListener(MoolandsMod::commonInit);
		modBus.addListener(MoolandsEntityTypes::registerAttributes);
		modBus.addListener(EventPriority.LOWEST, MoolandsEntityTypes::registerPlacements);

		forgeBus.register(MoolandsEvents.class);
	}

	private static void commonInit(final FMLCommonSetupEvent event)
	{
	}

	public static void clientInit(FMLClientSetupEvent event)
	{
		MoolandsClientEvents.initDimensionRenderInfo();
	}
}
