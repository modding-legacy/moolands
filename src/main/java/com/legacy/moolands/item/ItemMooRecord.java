package com.legacy.moolands.item;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.RecordItem;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class ItemMooRecord extends RecordItem
{
	public ItemMooRecord(SoundEvent soundIn, Item.Properties builder)
	{
		super(1, () -> soundIn, builder, 80);
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public MutableComponent getDisplayName()
	{
		return Component.literal("Jon Lachney - Moo");
	}
}