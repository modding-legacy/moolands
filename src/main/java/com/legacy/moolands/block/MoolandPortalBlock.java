package com.legacy.moolands.block;

import com.legacy.moolands.client.particle.data.MoolandDustOptions;
import com.legacy.moolands.registry.MoolandsDimensions;
import com.legacy.moolands.registry.MoolandsPoiTypes;
import com.legacy.structure_gel.api.block.GelPortalBlock;
import com.legacy.structure_gel.api.dimension.portal.GelTeleporter;
import com.legacy.structure_gel.api.dimension.portal.GelTeleporter.CreatePortalBehavior;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class MoolandPortalBlock extends GelPortalBlock
{
	public static final MapCodec<MoolandPortalBlock> CODEC = simpleCodec(MoolandPortalBlock::new);

	@Override
	public MapCodec<? extends MoolandPortalBlock> codec()
	{
		return CODEC;
	}

	public MoolandPortalBlock(Properties props)
	{
		super(props);
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void animateTick(BlockState stateIn, Level worldIn, BlockPos pos, RandomSource rand)
	{
		if (rand.nextInt(100) == 0)
			worldIn.playLocalSound((double) pos.getX() + 0.5D, (double) pos.getY() + 0.5D, (double) pos.getZ() + 0.5D, SoundEvents.PORTAL_AMBIENT, SoundSource.BLOCKS, 0.5F, rand.nextFloat() * 0.4F + 0.8F, false);

		for (int i = 0; i < 4; ++i)
		{
			double d0 = (double) pos.getX() + rand.nextDouble();
			double d1 = (double) pos.getY() + rand.nextDouble();
			double d2 = (double) pos.getZ() + rand.nextDouble();
			double d3 = ((double) rand.nextFloat() - 0.5D) * 0.5D;
			double d4 = ((double) rand.nextFloat() - 0.5D) * 0.5D;
			double d5 = ((double) rand.nextFloat() - 0.5D) * 0.5D;
			int j = rand.nextInt(2) * 2 - 1;

			if (!worldIn.getBlockState(pos.west()).is(this) && !worldIn.getBlockState(pos.east()).is(this))
			{
				d0 = (double) pos.getX() + 0.5D + 0.25D * (double) j;
				d3 = (double) (rand.nextFloat() * 2.0F * (float) j);
			}
			else
			{
				d2 = (double) pos.getZ() + 0.5D + 0.25D * (double) j;
				d5 = (double) (rand.nextFloat() * 2.0F * (float) j);
			}

			worldIn.addParticle(MoolandDustOptions.MILKY_PORTAL, d0, d1, d2, d3, d4, d5);
		}
	}

	@Override
	public GelTeleporter getTeleporter(ServerLevel serverLevel, GelPortalBlock portal)
	{
		return new GelTeleporter(serverLevel, () -> Level.OVERWORLD, MoolandsDimensions::moolandsKey, MoolandsPoiTypes.MOOLAND_PORTAL::get, () -> portal, Blocks.GLOWSTONE::defaultBlockState, CreatePortalBehavior.ON_SURFACE);
	}
}