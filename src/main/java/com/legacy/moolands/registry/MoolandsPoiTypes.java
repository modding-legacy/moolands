package com.legacy.moolands.registry;

import java.util.Arrays;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.legacy.moolands.MoolandsMod;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.entity.ai.village.poi.PoiType;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.neoforge.registries.RegisterEvent;

public class MoolandsPoiTypes
{
	public static final RegistrarHandler<PoiType> HANDLER = RegistrarHandler.getOrCreate(Registries.POINT_OF_INTEREST_TYPE, MoolandsMod.MODID);

	public static final Registrar.Static<PoiType> MOOLAND_PORTAL = withTicketCount("everbright_portal", () -> MoolandsBlocks.mooland_portal, 0);

	public static void init(RegisterEvent event)
	{
		HANDLER.registerValues(event);
	}

	private static Registrar.Static<PoiType> withTicketCount(String name, Supplier<Block> block, int ticket)
	{
		return HANDLER.createStatic(name, () -> new PoiType(getAllStates(block.get()), ticket, 1));
	}

	private static Set<BlockState> getAllStates(Block block)
	{
		return Set.copyOf(block.getStateDefinition().getPossibleStates());
	}

	public static Set<BlockState> getAllStates(Block... blocks)
	{
		return Arrays.stream(blocks).flatMap(b -> getAllStates(b).stream()).collect(Collectors.toSet());
	}
}
