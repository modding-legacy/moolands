package com.legacy.moolands.world.feature;

import com.legacy.moolands.registry.MoolandsBlocks;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.BushBlock;
import net.minecraft.world.level.block.HugeMushroomBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.HugeMushroomFeatureConfiguration;

public class BigBlueMushroomFeature<FC extends HugeMushroomFeatureConfiguration> extends Feature<FC>
{
	public BigBlueMushroomFeature(Codec<FC> config)
	{
		super(config);
	}

	@Override
	public boolean place(FeaturePlaceContext<FC> context)
	{
		WorldGenLevel level = context.level();
		RandomSource rand = level.getRandom();
		BlockPos pos = context.origin();

		boolean largeMushroom = rand.nextInt(4) == 0;

		int mushroomWidth = largeMushroom ? 4 : 2;
		int mushroomHeight = largeMushroom ? 6 : 3;
		int mushroomTopWidth = largeMushroom ? 3 : 1;

		int i = rand.nextInt(3) + 4 + (largeMushroom ? 3 : 0);
		if (rand.nextInt(12) == 0)
		{
			i *= 2;
		}

		int j = pos.getY();
		if (j >= 1 && j + i + 1 < 256)
		{
			Block block = level.getBlockState(pos.below()).getBlock();

			if (block != MoolandsBlocks.mooland_dirt && block != MoolandsBlocks.mooland_grass_block)
			{
				return false;
			}
			else
			{
				BlockPos.MutableBlockPos mutablePos = new BlockPos.MutableBlockPos();

				for (int k = 0; k <= i; ++k)
				{
					int l = 0;
					if (k < i && k >= i - 3)
					{
						l = 2;
					}
					else if (k == i)
					{
						l = 1;
					}

					for (int i1 = -l; i1 <= l; ++i1)
					{
						for (int j1 = -l; j1 <= l; ++j1)
						{
							BlockState blockstate = level.getBlockState(mutablePos.set(pos).move(i1, k, j1));
							if (!blockstate.isAir() && !blockstate.is(BlockTags.LEAVES))
							{
								return false;
							}
						}
					}
				}

				BlockState blockstate1 = MoolandsBlocks.blue_mushroom_block.defaultBlockState().setValue(HugeMushroomBlock.DOWN, Boolean.valueOf(false));

				for (int l1 = i - mushroomHeight; l1 <= i; ++l1)
				{
					int i2 = l1 < i ? mushroomWidth : mushroomTopWidth;

					for (int l2 = -i2; l2 <= i2; ++l2)
					{
						for (int k1 = -i2; k1 <= i2; ++k1)
						{
							boolean flag = l2 == -i2;
							boolean flag1 = l2 == i2;
							boolean flag2 = k1 == -i2;
							boolean flag3 = k1 == i2;
							boolean flag4 = flag || flag1;
							boolean flag5 = flag2 || flag3;
							if (l1 >= i || flag4 != flag5)
							{
								mutablePos.set(pos).move(l2, l1, k1);
								if (!level.getBlockState(mutablePos).isSolidRender(level, mutablePos))
								{
									this.setBlock(level, mutablePos, blockstate1.setValue(HugeMushroomBlock.UP, Boolean.valueOf(l1 >= i - 1)).setValue(HugeMushroomBlock.WEST, Boolean.valueOf(l2 < 0)).setValue(HugeMushroomBlock.EAST, Boolean.valueOf(l2 > 0)).setValue(HugeMushroomBlock.NORTH, Boolean.valueOf(k1 < 0)).setValue(HugeMushroomBlock.SOUTH, Boolean.valueOf(k1 > 0)));
								}
							}
						}
					}
				}

				BlockState blockstate2 = Blocks.MUSHROOM_STEM.defaultBlockState().setValue(HugeMushroomBlock.UP, Boolean.valueOf(false)).setValue(HugeMushroomBlock.DOWN, Boolean.valueOf(false));

				for (int j2 = 0; j2 < i; ++j2)
				{
					if (largeMushroom)
					{
						for (int l2 = -1; l2 <= 1; ++l2)
						{
							for (int k2 = -1; k2 <= 1; ++k2)
							{
								mutablePos.set(pos).move(Direction.UP, j2).move(l2, 0, k2);
								if (!level.getBlockState(mutablePos).isSolidRender(level, mutablePos) || level.getBlockState(mutablePos).getBlock() instanceof BushBlock)
								{
									this.setBlock(level, mutablePos, blockstate2);
								}
							}
						}
					}
					else
					{
						mutablePos.set(pos).move(Direction.UP, j2);
						if (!level.getBlockState(mutablePos).isSolidRender(level, mutablePos))
						{
							this.setBlock(level, mutablePos, blockstate2);
						}
					}

				}

				return true;
			}
		}
		else
		{
			return false;
		}
	}
}