package com.legacy.moolands;

import java.util.List;

import com.legacy.moolands.registry.MoolandsBlocks;
import com.legacy.structure_gel.api.dimension.portal.GelPortalLogic;

import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.MilkBucketItem;
import net.minecraft.world.level.block.Blocks;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.event.entity.player.PlayerInteractEvent.RightClickBlock;

public abstract class MoolandsEvents
{
	@SubscribeEvent
	public static void onRightClickBlock(RightClickBlock event)
	{
		if (!event.getItemStack().isEmpty() && event.getItemStack().getItem() instanceof MilkBucketItem)
		{
			if (GelPortalLogic.fillPortal(event.getLevel(), event.getPos().relative(event.getFace()), MoolandsBlocks.mooland_portal, List.of(Blocks.AIR)))
			{
				ItemStack stack = event.getItemStack();
				Player player = event.getEntity();

				event.setCanceled(true);
				player.playSound(SoundEvents.COW_MILK, 1.0F, 1.0F);
				player.swing(event.getHand());

				if (!player.isCreative())
					player.setItemInHand(event.getHand(), stack.hasCraftingRemainingItem() ? stack.getCraftingRemainingItem() : ItemStack.EMPTY);
			}
		}
	}
}