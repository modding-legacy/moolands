package com.legacy.moolands.world.structure;

import com.legacy.moolands.MoolandsMod;
import com.legacy.moolands.registry.MoolandsBlocks;
import com.legacy.moolands.registry.MoolandsStructures;
import com.legacy.structure_gel.api.structure.GelTemplateStructurePiece;
import com.legacy.structure_gel.api.structure.processor.RemoveGelStructureProcessor;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.StructureManager;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.RandomState;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceSerializationContext;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePiecesBuilder;
import net.minecraft.world.level.levelgen.structure.templatesystem.BlockIgnoreProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplateManager;

public class MushroomCampPieces
{
	private static final ResourceLocation YELLOW_CAMP = locatePiece("mushroom_camp");

	public static void assemble(StructureTemplateManager templateManager, BlockPos pos, Rotation rotation, StructurePiecesBuilder builder, RandomSource random, RandomState randomState)
	{
		builder.addPiece(new MushroomCampPieces.Piece(templateManager, YELLOW_CAMP, pos.below(), rotation));
	}

	static ResourceLocation locatePiece(String location)
	{
		return MoolandsMod.locate(location);
	}

	public static class Piece extends GelTemplateStructurePiece
	{
		public Piece(StructureTemplateManager structureManager, ResourceLocation name, BlockPos pos, Rotation rotation)
		{
			super(MoolandsStructures.MUSHROOM_CAMP.getPieceType("main").get(), 0, structureManager, name, Piece.getPlacementSettings(structureManager, name, pos, rotation), pos);
			this.rotation = rotation;
		}

		public Piece(StructurePieceSerializationContext context, CompoundTag nbt)
		{
			super(MoolandsStructures.MUSHROOM_CAMP.getPieceType("main").get(), nbt, context.structureTemplateManager(), name -> Piece.getPlacementSettings(context.structureTemplateManager(), name, new BlockPos(nbt.getInt("TPX"), nbt.getInt("TPY"), nbt.getInt("TPZ")), Rotation.valueOf(nbt.getString("Rot"))));
		}

		private static StructurePlaceSettings getPlacementSettings(StructureTemplateManager structureManager, ResourceLocation name, BlockPos pos, Rotation rotation)
		{
			Vec3i sizePos = structureManager.get(name).get().getSize();
			BlockPos centerPos = new BlockPos(sizePos.getX() / 2, 0, sizePos.getZ() / 2);

			StructurePlaceSettings placementSettings = new StructurePlaceSettings().setKeepLiquids(false).setRotation(rotation).setMirror(Mirror.NONE).setRotationPivot(centerPos);
			placementSettings.addProcessor(BlockIgnoreProcessor.STRUCTURE_BLOCK);
			placementSettings.addProcessor(RemoveGelStructureProcessor.INSTANCE);

			return placementSettings;
		}

		@Override
		public void postProcess(WorldGenLevel pLevel, StructureManager structureManager, ChunkGenerator chunkGeneratorIn, RandomSource randomIn, BoundingBox bounds, ChunkPos chunkPosIn, BlockPos pos)
		{
			super.postProcess(pLevel, structureManager, chunkGeneratorIn, randomIn, bounds, chunkPosIn, pos);

			pos = pos.below();
			// failsafe to hide stem, or if it's floating
			for (int y = 0; y <= 4; ++y)
			{
				int width = y == 0 ? 2 : y == 1 ? 3 : y == 2 ? 4 : 5;
				for (int x = -width; x <= width; ++x)
				{
					for (int z = -width; z <= width; ++z)
					{
						BlockPos fillPos = pos.offset(x, y - 3, z);
						if (!(Math.abs(x) == width && Math.abs(x) == Math.abs(z)) && pLevel.getBlockState(fillPos).canBeReplaced())
							pLevel.setBlock(fillPos, y == 4 ? MoolandsBlocks.mooland_grass_block.defaultBlockState() : MoolandsBlocks.mooland_dirt.defaultBlockState(), 2);
					}
				}
			}
		}

		@Override
		protected void handleDataMarker(String key, BlockPos pos, ServerLevelAccessor level, RandomSource rand, BoundingBox sbb)
		{
		}
	}
}
