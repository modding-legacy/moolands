package com.legacy.moolands.world.gen;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import com.legacy.moolands.registry.MoolandsBlocks;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.SharedConstants;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderLookup;
import net.minecraft.server.level.WorldGenRegion;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.LevelHeightAccessor;
import net.minecraft.world.level.NaturalSpawner;
import net.minecraft.world.level.NoiseColumn;
import net.minecraft.world.level.StructureManager;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeManager;
import net.minecraft.world.level.biome.BiomeSource;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.chunk.ChunkGeneratorStructureState;
import net.minecraft.world.level.levelgen.GenerationStep.Carving;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraft.world.level.levelgen.LegacyRandomSource;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;
import net.minecraft.world.level.levelgen.RandomState;
import net.minecraft.world.level.levelgen.RandomSupport;
import net.minecraft.world.level.levelgen.WorldgenRandom;
import net.minecraft.world.level.levelgen.WorldgenRandom.Algorithm;
import net.minecraft.world.level.levelgen.blending.Blender;
import net.minecraft.world.level.levelgen.structure.StructureSet;
import net.minecraft.world.level.levelgen.synth.SimplexNoise;

public class StrangeChunkGenerator extends ChunkGenerator
{
	public static final Codec<StrangeChunkGenerator> STRANGE_CODEC = RecordCodecBuilder.create(instance ->
	{
		return instance.group(BiomeSource.CODEC.fieldOf("biome_source").forGetter(chunkGen ->
		{
			return chunkGen.biomeSource;
		}), NoiseGeneratorSettings.CODEC.fieldOf("settings").forGetter(chunkGen ->
		{
			return chunkGen.settings;
		})).apply(instance, instance.stable(StrangeChunkGenerator::new));
	});

	public static final BlockState AIR = Blocks.AIR.defaultBlockState();

	private final Holder<NoiseGeneratorSettings> settings;

	public SimplexNoise floatingDiscNoise = null;

	private final float shortXZScale = 110.0F, shortYScale = 12.0F, tallYScale = 20.0F;

	public StrangeChunkGenerator(BiomeSource biomeProvider, Holder<NoiseGeneratorSettings> settings)
	{
		super(biomeProvider);

		this.settings = settings;
	}

	@Override
	protected Codec<? extends ChunkGenerator> codec()
	{
		return STRANGE_CODEC;
	}

	@Override
	public ChunkGeneratorStructureState createState(HolderLookup<StructureSet> structureRegistry, RandomState rand, long serverSeed)
	{
		this.floatingDiscNoise = new SimplexNoise(new WorldgenRandom(Algorithm.LEGACY.newInstance(serverSeed)));
		return super.createState(structureRegistry, rand, serverSeed);
	}

	@Override
	public void buildSurface(WorldGenRegion level, StructureManager pStructureManager, RandomState randomState, ChunkAccess chunkAccess)
	{
		if (!SharedConstants.debugVoidTerrain(chunkAccess.getPos()))
			this.replaceBlocksForBiome(chunkAccess.getPos().x, chunkAccess.getPos().z, randomState, chunkAccess);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void spawnOriginalMobs(WorldGenRegion level)
	{
		if (!this.settings.value().disableMobGeneration())
		{
			ChunkPos chunkpos = level.getCenter();
			Holder<Biome> holder = level.getBiome(chunkpos.getWorldPosition().atY(level.getMaxBuildHeight() - 1));
			WorldgenRandom worldgenrandom = new WorldgenRandom(new LegacyRandomSource(RandomSupport.generateUniqueSeed()));
			worldgenrandom.setDecorationSeed(level.getSeed(), chunkpos.getMinBlockX(), chunkpos.getMinBlockZ());
			NaturalSpawner.spawnMobsForChunkGeneration(level, holder, chunkpos, worldgenrandom);
		}
	}

	@Override
	public CompletableFuture<ChunkAccess> fillFromNoise(Executor executor, Blender blender, RandomState randomState, StructureManager structureManager, ChunkAccess chunkAccess)
	{
		BlockPos cpos = chunkAccess.getPos().getWorldPosition();
		int chunkWidth = 16;

		for (int x = 0; x < chunkWidth; ++x)
		{
			int posX = cpos.getX() + x;

			for (int z = 0; z < chunkWidth; ++z)
			{
				int posZ = cpos.getZ() + z;

				for (int posY = 200; posY > 0; --posY)
				{
					BlockState state = this.getStateAt(posX, posY, posZ);

					if (state != AIR)
						chunkAccess.setBlockState(new BlockPos(x, posY, z), state, false);
				}
			}
		}

		return CompletableFuture.completedFuture(chunkAccess);
	}

	private void replaceBlocksForBiome(int chunkX, int chunkZ, RandomState randomState, ChunkAccess chunk)
	{
		for (int k = 0; k < 16; k++)
		{
			for (int l = 0; l < 16; l++)
			{
				int i1 = (int) (2.0D + randomState.oreRandom().at(k, 0, l).nextDouble() * 0.25D);

				int j1 = -1;

				BlockState top = MoolandsBlocks.mooland_grass_block.defaultBlockState();
				BlockState filler = MoolandsBlocks.mooland_dirt.defaultBlockState();

				for (int k1 = 200; k1 >= 0; k1--)
				{
					Block block = chunk.getBlockState(new BlockPos(k, k1, l)).getBlock();

					if (block == Blocks.AIR)
					{
						j1 = -1;
					}
					else if (block == MoolandsBlocks.moo_rock)
					{
						if (j1 == -1)
						{
							if (i1 <= 0)
							{
								top = Blocks.AIR.defaultBlockState();
								filler = MoolandsBlocks.moo_rock.defaultBlockState();
							}

							j1 = i1;

							chunk.setBlockState(new BlockPos(k, k1, l), k1 >= 0 ? top : filler, false);
						}
						else if (j1 > 0)
						{
							--j1;
							chunk.setBlockState(new BlockPos(k, k1, l), filler, false);
						}
					}
				}
			}
		}
	}

	private BlockState getStateAt(int worldX, int worldY, int worldZ)
	{
		BlockState state = AIR;
		boolean low = worldY <= 40;

		if (worldY <= 200 && this.floatingDiscNoise.getValue(worldX / this.shortXZScale, worldY / (low ? this.tallYScale : this.shortYScale), worldZ / this.shortXZScale) < (low ? -0.4F : -0.7F))
			state = MoolandsBlocks.moo_rock.defaultBlockState();

		return state;
	}

	@Override
	public int getGenDepth()
	{
		return this.settings.value().noiseSettings().height();
	}

	@Override
	public int getSeaLevel()
	{
		return this.settings.value().seaLevel();
	}

	@Override
	public int getMinY()
	{
		return this.settings.value().noiseSettings().minY();
	}

	@Override
	public int getBaseHeight(int x, int z, Types pType, LevelHeightAccessor level, RandomState pRandom)
	{
		return level.getMinBuildHeight() + this.iterateColumn(x, z, new ArrayList<>(80), null, level);
	}

	@Override
	public NoiseColumn getBaseColumn(int x, int z, LevelHeightAccessor level, RandomState randomState)
	{
		List<BlockState> states = new ArrayList<>(100);
		this.iterateColumn(x, z, states, null, level);
		return new NoiseColumn(level.getMinBuildHeight(), states.toArray(BlockState[]::new));
	}

	private int iterateColumn(int worldX, int worldZ, List<BlockState> states, @Nullable Predicate<BlockState> stateTest, LevelHeightAccessor level)
	{
		int minY = level.getMinBuildHeight();
		int worldY = level.getMaxBuildHeight();
		int topY = 0;

		while (worldY > minY)
		{
			BlockState state = this.getStateAt(worldX, worldY, worldZ);

			if (state.isAir())
				worldY--;
			else
			{
				topY = worldY;
				/*System.out.println(worldX + " " + worldY + " " + worldZ + ": " + state);*/
				break;
			}
		}

		worldY = level.getMinBuildHeight();

		while (worldY < topY)
		{
			BlockState state = this.getStateAt(worldX, worldY, worldZ);

			if (stateTest == null || stateTest.test(state))
			{
				states.add(state);
			}
			else
				break;

			worldY++;
		}

		return states.size();
	}

	@Override
	public void addDebugScreenInfo(List<String> pInfo, RandomState pRandom, BlockPos pPos)
	{
	}

	@Override
	public void applyCarvers(WorldGenRegion pLevel, long pSeed, RandomState pRandom, BiomeManager pBiomeManager, StructureManager pStructureManager, ChunkAccess pChunk, Carving pStep)
	{
	}
}