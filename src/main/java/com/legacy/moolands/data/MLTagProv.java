package com.legacy.moolands.data;

import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Stream;

import com.legacy.moolands.MoolandsMod;
import com.legacy.moolands.registry.MoolandsBlocks;

import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.FlowerBlock;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.GrassBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.WallBlock;
import net.neoforged.neoforge.common.Tags;
import net.neoforged.neoforge.common.data.BlockTagsProvider;
import net.neoforged.neoforge.common.data.ExistingFileHelper;

public class MLTagProv
{
	public static class BlockTagProv extends BlockTagsProvider
	{
		public BlockTagProv(DataGenerator generatorIn, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, MoolandsMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider prov)
		{
			vanilla();
			forge();
		}

		void vanilla()
		{
			addMatching(BlockTags.FLOWER_POTS, b -> b instanceof FlowerPotBlock);
			addMatching(BlockTags.SLABS, b -> b instanceof SlabBlock);
			addMatching(BlockTags.STAIRS, b -> b instanceof StairBlock);
			addMatching(BlockTags.WALLS, b -> b instanceof WallBlock);
			addMatching(BlockTags.SMALL_FLOWERS, b -> b instanceof FlowerBlock);
			addMatching(BlockTags.VALID_SPAWN, b -> b instanceof GrassBlock);

			this.tag(BlockTags.STONE_BRICKS).add(MoolandsBlocks.moo_rock_bricks);

			this.tag(BlockTags.MINEABLE_WITH_PICKAXE).add(MoolandsBlocks.moo_rock, MoolandsBlocks.moo_rock_slab, MoolandsBlocks.moo_rock_stairs, MoolandsBlocks.moo_rock_wall, MoolandsBlocks.moo_rock_bricks, MoolandsBlocks.moo_rock_brick_slab, MoolandsBlocks.moo_rock_brick_stairs, MoolandsBlocks.moo_rock_brick_wall, MoolandsBlocks.moo_rock_iron_ore);
			this.tag(BlockTags.MINEABLE_WITH_AXE).add(MoolandsBlocks.blue_mushroom_block, MoolandsBlocks.yellow_mushroom_block);
			this.tag(BlockTags.MINEABLE_WITH_SHOVEL).add(MoolandsBlocks.mooland_grass_block, MoolandsBlocks.mooland_dirt);
			this.tag(BlockTags.MUSHROOM_GROW_BLOCK).add(MoolandsBlocks.mooland_grass_block, MoolandsBlocks.mooland_dirt);
			this.tag(BlockTags.NEEDS_STONE_TOOL).add(MoolandsBlocks.moo_rock_iron_ore);
		}

		void forge()
		{
			this.tag(Tags.Blocks.STONE).add(MoolandsBlocks.moo_rock);
		}

		private Stream<Block> getMatching(Function<Block, Boolean> condition)
		{
			return BuiltInRegistries.BLOCK.stream().filter(block -> BuiltInRegistries.BLOCK.getKey(block).getNamespace().equals(MoolandsMod.MODID) && condition.apply(block));
		}

		private void addMatching(TagKey<Block> blockTag, Function<Block, Boolean> condition)
		{
			getMatching(condition).forEach(this.tag(blockTag)::add);
		}

		@Override
		public String getName()
		{
			return MoolandsMod.MODID + " Block Tags";
		}
	}

	public static class ItemTagProv extends ItemTagsProvider
	{
		public ItemTagProv(DataGenerator gen, CompletableFuture<TagLookup<Block>> blockTagProv, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(gen.getPackOutput(), lookup, blockTagProv, MoolandsMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider prov)
		{
			vanilla();
			forge();
		}

		void vanilla()
		{
			this.copy(BlockTags.SLABS, ItemTags.SLABS);
			this.copy(BlockTags.STAIRS, ItemTags.STAIRS);
			this.copy(BlockTags.WALLS, ItemTags.WALLS);
			this.copy(BlockTags.SMALL_FLOWERS, ItemTags.SMALL_FLOWERS);
			this.copy(BlockTags.STONE_BRICKS, ItemTags.STONE_BRICKS);
			
			this.tag(ItemTags.STONE_TOOL_MATERIALS).add(MoolandsBlocks.moo_rock.asItem());
			this.tag(ItemTags.STONE_CRAFTING_MATERIALS).add(MoolandsBlocks.moo_rock.asItem());
		}

		void forge()
		{
			this.tag(Tags.Items.MUSHROOMS).add(MoolandsBlocks.blue_mushroom.asItem(), MoolandsBlocks.yellow_mushroom.asItem());

			this.copy(Tags.Blocks.STONE, Tags.Items.STONE);
		}

		@Override
		public String getName()
		{
			return MoolandsMod.MODID + " Item Tags";
		}
	}
}
