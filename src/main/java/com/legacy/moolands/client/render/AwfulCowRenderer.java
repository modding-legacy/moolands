package com.legacy.moolands.client.render;

import com.legacy.moolands.MoolandsMod;
import com.legacy.moolands.client.render.models.AwfulCowModel;
import com.legacy.moolands.entity.AwfulCowEntity;
import com.legacy.moolands.registry.MoolandsDimensions;

import net.minecraft.client.model.geom.ModelLayers;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;

public class AwfulCowRenderer<T extends AwfulCowEntity, M extends AwfulCowModel<T>> extends MobRenderer<T, M>
{
	private static final ResourceLocation YELLOW_COW = MoolandsMod.locate("textures/entity/yellow_cow.png");
	private static final ResourceLocation BLUE_COW = MoolandsMod.locate("textures/entity/blue_cow.png");
	private static final ResourceLocation JONATHING_COW = MoolandsMod.locate("textures/entity/jonathing.png");

	@SuppressWarnings("unchecked")
	public AwfulCowRenderer(EntityRendererProvider.Context context)
	{
		super(context, (M) new AwfulCowModel<>(context.bakeLayer(ModelLayers.COW)), 0.7F);
		this.addLayer(new AwfulSaddleLayer<>(this, new AwfulCowModel<>(context.bakeLayer(AwfulCowModel.SADDLE_LAYER))));
	}

	@Override
	protected boolean isShaking(T entity)
	{
		return entity.level().dimension() != MoolandsDimensions.moolandsKey();
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return entity.isJonathing() ? JONATHING_COW : entity.getCowType() == 1 ? YELLOW_COW : BLUE_COW;
	}
}
