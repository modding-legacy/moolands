package com.legacy.moolands.registry;

import com.legacy.moolands.MoolandsMod;
import com.legacy.moolands.item.ItemMooRecord;
import com.legacy.moolands.world.gen.StrangeChunkGenerator;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Rarity;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.common.DeferredSpawnEggItem;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.registries.RegisterEvent;

public class MoolandsRegistry
{
	public static final Lazy<Item> AWFUL_COW_SPAWN_EGG = Lazy.of(() -> new DeferredSpawnEggItem(() -> MoolandsEntityTypes.AWFUL_COW, 0x33599c, 0xd4cd37, new Item.Properties()));
	public static final Lazy<Item> MOO_DISC = Lazy.of(() -> new ItemMooRecord(MoolandsSounds.MOO_DISC, (new Item.Properties()).stacksTo(1).rarity(Rarity.RARE)));

	@SubscribeEvent
	public static void onRegistry(RegisterEvent event)
	{
		if (event.getRegistryKey().equals(Registries.ENTITY_TYPE))
			MoolandsEntityTypes.init(event);
		else if (event.getRegistryKey().equals(Registries.ITEM))
		{
			event.register(Registries.ITEM, MoolandsMod.locate("awful_cow_spawn_egg"), AWFUL_COW_SPAWN_EGG);
			event.register(Registries.ITEM, MoolandsMod.locate("moo_disc"), MOO_DISC);

			MoolandsBlocks.blockList.forEach((block) -> event.register(Registries.ITEM, MoolandsMod.locate(BuiltInRegistries.BLOCK.getKey(block).getPath()), () -> new BlockItem(block, new Item.Properties())));
			MoolandsBlocks.blockList.clear();

			MoolandsBlocks.blockItemPropertiesMap.entrySet().forEach((entry) -> event.register(Registries.ITEM, MoolandsMod.locate(BuiltInRegistries.BLOCK.getKey(entry.getKey()).getPath()), () -> new BlockItem(entry.getKey(), entry.getValue())));
			MoolandsBlocks.blockItemPropertiesMap.clear();
		}
		else if (event.getRegistryKey().equals(Registries.BLOCK))
			MoolandsBlocks.init(event);
		else if (event.getRegistryKey().equals(Registries.SOUND_EVENT))
			MoolandsSounds.init();
		else if (event.getRegistryKey().equals(Registries.POINT_OF_INTEREST_TYPE))
			MoolandsPoiTypes.init(event);
		else if (event.getRegistryKey().equals(Registries.FEATURE))
		{
			MoolandsFeatures.init(event);
		}
		else if (event.getRegistryKey().equals(Registries.PARTICLE_TYPE))
			com.legacy.moolands.registry.MoolandsParticles.init(event);
		else if (event.getRegistryKey().equals(Registries.CHUNK_GENERATOR))
			event.register(Registries.CHUNK_GENERATOR, MoolandsMod.locate("strange_noise"), () -> StrangeChunkGenerator.STRANGE_CODEC);
	}
}