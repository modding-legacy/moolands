package com.legacy.moolands.client.particle;

import com.legacy.moolands.client.particle.data.MoolandDustOptions;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.PortalParticle;
import net.minecraft.client.particle.SpriteSet;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class MoolandDustParticle extends PortalParticle
{
	public MoolandDustParticle(ClientLevel pLevel, double pX, double pY, double pZ, double pXSpeed, double pYSpeed, double pZSpeed, MoolandDustOptions options)
	{
		super(pLevel, pX, pY, pZ, pXSpeed, pYSpeed, pZSpeed);

		if (options.isBlueTintOnly())
		{
			float range = this.random.nextFloat() * 0.6F + 0.4F;
			this.rCol = this.gCol = this.bCol = 1F * range;
			this.rCol *= 1F;
			this.gCol *= 1F;
		}
		else
		{
			float range = this.random.nextFloat() * 0.4F + 0.6F;
			this.rCol = this.randomizeColor(options.getColor().x(), range);
			this.gCol = this.randomizeColor(options.getColor().y(), range);
			this.bCol = this.randomizeColor(options.getColor().z(), range);
		}

		this.quadSize *= options.getScale();
	}

	protected float randomizeColor(float pCoordMultiplier, float pMultiplier)
	{
		return (this.random.nextFloat() * 0.2F + 0.8F) * pCoordMultiplier * pMultiplier;
	}

	@OnlyIn(Dist.CLIENT)
	public static class Factory implements ParticleProvider<MoolandDustOptions>
	{
		private final SpriteSet sprite;

		public Factory(SpriteSet pSprites)
		{
			this.sprite = pSprites;
		}

		@Override
		public Particle createParticle(MoolandDustOptions pType, ClientLevel pLevel, double pX, double pY, double pZ, double pXSpeed, double pYSpeed, double pZSpeed)
		{
			MoolandDustParticle reverseportalparticle = new MoolandDustParticle(pLevel, pX, pY, pZ, pXSpeed, pYSpeed, pZSpeed, pType);
			reverseportalparticle.pickSprite(this.sprite);
			return reverseportalparticle;
		}
	}
}
