package com.legacy.moolands.world;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.legacy.moolands.registry.MoolandsBlocks;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.SurfaceRules;

public class MoolandsSurfaceRuleData
{
	private static final SurfaceRules.RuleSource MOO_GRASS = stateRule(MoolandsBlocks.mooland_grass_block);
	private static final SurfaceRules.RuleSource MOO_DIRT = stateRule(MoolandsBlocks.mooland_dirt);

	public static SurfaceRules.RuleSource create(boolean preliminarySurface)
	{
		SurfaceRules.RuleSource belowSurfaceSource = SurfaceRules.sequence(MOO_DIRT);
		SurfaceRules.RuleSource onSurfaceSource = SurfaceRules.sequence(MOO_GRASS);

		// @formatter:off
		SurfaceRules.RuleSource floorModifiers = SurfaceRules.sequence(
				SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, onSurfaceSource),
				SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, belowSurfaceSource)
				);
		// @formatter:on

		Builder<SurfaceRules.RuleSource> builder = ImmutableList.builder();

		builder.add(preliminarySurface ? SurfaceRules.ifTrue(SurfaceRules.abovePreliminarySurface(), floorModifiers) : floorModifiers);

		return SurfaceRules.sequence(builder.build().toArray((source) -> new SurfaceRules.RuleSource[source]));
	}

	private static SurfaceRules.RuleSource stateRule(Block block)
	{
		return stateRule(block.defaultBlockState());
	}

	private static SurfaceRules.RuleSource stateRule(BlockState state)
	{
		return SurfaceRules.state(state);
	}
}
