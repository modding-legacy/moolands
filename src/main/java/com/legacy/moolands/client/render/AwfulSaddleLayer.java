package com.legacy.moolands.client.render;

import com.legacy.moolands.MoolandsMod;
import com.legacy.moolands.client.render.models.AwfulCowModel;
import com.legacy.moolands.entity.AwfulCowEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.RenderLayer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;

public class AwfulSaddleLayer<T extends AwfulCowEntity, M extends AwfulCowModel<T>> extends RenderLayer<T, M>
{
	private static final ResourceLocation SADDLE = MoolandsMod.locate("textures/entity/saddle.png");
	private final AwfulCowModel<T> cowModel;

	public AwfulSaddleLayer(RenderLayerParent<T, M> parent, AwfulCowModel<T> saddleModel)
	{
		super(parent);
		this.cowModel = saddleModel;
	}

	@Override
	public void render(PoseStack pose, MultiBufferSource bufferIn, int packedLight, T entity, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
	{
		if (entity.isSaddled())
		{
			this.getParentModel().copyPropertiesTo(this.cowModel);
			this.cowModel.prepareMobModel(entity, limbSwing, limbSwingAmount, partialTicks);
			this.cowModel.setupAnim(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
			VertexConsumer ivertexbuilder = bufferIn.getBuffer(RenderType.entityCutoutNoCull(SADDLE));
			this.cowModel.renderToBuffer(pose, ivertexbuilder, packedLight, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
		}
	}
}