package com.legacy.moolands.block;

import java.util.function.Supplier;

import javax.annotation.Nullable;

import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;

public class MoolandsFlowerPotBlock extends FlowerPotBlock
{
	public static final MapCodec<FlowerPotBlock> CODEC = RecordCodecBuilder.mapCodec(instance -> instance.group(flowerCodec(), propertiesCodec()).apply(instance, (flower, prop) -> (FlowerPotBlock) new MoolandsFlowerPotBlock(() -> flower, prop)));

	@Override
	public MapCodec<FlowerPotBlock> codec()
	{
		return CODEC;
	}

	public MoolandsFlowerPotBlock(@Nullable Supplier<FlowerPotBlock> emptyPot, Supplier<? extends Block> flower, BlockBehaviour.Properties properties)
	{
		super(emptyPot, flower, properties);
		((FlowerPotBlock) Blocks.FLOWER_POT).addPlant(BuiltInRegistries.BLOCK.getKey(flower.get()), () -> this);
	}

	public MoolandsFlowerPotBlock(Supplier<? extends Block> flower, BlockBehaviour.Properties properties)
	{
		this(() -> (FlowerPotBlock) Blocks.FLOWER_POT, flower, properties);
	}

	public MoolandsFlowerPotBlock(java.util.function.Supplier<? extends Block> flower)
	{
		this(flower, BlockBehaviour.Properties.of().strength(0.0F));
	}

	protected static <T extends FlowerPotBlock> RecordCodecBuilder<T, Block> flowerCodec()
	{
		return BuiltInRegistries.BLOCK.byNameCodec().fieldOf("potted").forGetter(block -> block.getPotted());
	}

}
