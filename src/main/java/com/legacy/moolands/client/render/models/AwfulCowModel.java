package com.legacy.moolands.client.render.models;

import com.legacy.moolands.MoolandsMod;
import com.legacy.moolands.entity.AwfulCowEntity;

import net.minecraft.client.model.CowModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;

public class AwfulCowModel<T extends AwfulCowEntity> extends CowModel<T>
{
	public static final ModelLayerLocation SADDLE_LAYER = new ModelLayerLocation(MoolandsMod.locate("awful_cow"), "saddle");

	public AwfulCowModel(ModelPart model)
	{
		super(model);
		/*this.head = new ModelPart(this, 0, 0);
		this.head.addBox(-4F, -4F, -6F, 8, 8, 6, 0.0F);
		this.head.setPos(0.0F, 4F, -8F);
		this.head.texOffs(22, 0).addBox(-5F, -5F, -4F, 1, 3, 1, 0.0F);
		this.head.texOffs(22, 0).addBox(4F, -5F, -4F, 1, 3, 1, 0.0F);
		
		this.body = new ModelPart(this, 18, 4);
		this.body.addBox(-6F, -10F, -7F, 12, 18, 10, scale);
		this.body.setPos(0.0F, 5F, 2.0F);
		this.body.texOffs(52, 0).addBox(-2F, 2.0F, -8F, 4, 6, 1);
		
		leg2.mirror = true;
		leg0.mirror = true;
		
		this.attackTime += 2.0F;*/
	}

	public static LayerDefinition createSaddleLayer(CubeDeformation size)
	{
		MeshDefinition mesh = new MeshDefinition();
		PartDefinition root = mesh.getRoot();
		root.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-4.0F, -4.0F, -6.0F, 8.0F, 8.0F, 6.0F).texOffs(22, 0).addBox("right_horn", -5.0F, -5.0F, -4.0F, 1.0F, 3.0F, 1.0F).texOffs(22, 0).addBox("left_horn", 4.0F, -5.0F, -4.0F, 1.0F, 3.0F, 1.0F), PartPose.offset(0.0F, 4.0F, -8.0F));
		root.addOrReplaceChild("body", CubeListBuilder.create().texOffs(18, 4).addBox(-6.0F, -10.0F, -7.0F, 12.0F, 18.0F, 10.0F, size).texOffs(52, 0).addBox(-2.0F, 2.0F, -8.0F, 4.0F, 6.0F, 1.0F), PartPose.offsetAndRotation(0.0F, 5.0F, 2.0F, ((float) Math.PI / 2F), 0.0F, 0.0F));
		CubeListBuilder cubelistbuilder = CubeListBuilder.create().texOffs(0, 16).addBox(-2.0F, 0.0F, -2.0F, 4.0F, 12.0F, 4.0F);
		root.addOrReplaceChild("right_hind_leg", cubelistbuilder, PartPose.offset(-4.0F, 12.0F, 7.0F));
		root.addOrReplaceChild("left_hind_leg", cubelistbuilder, PartPose.offset(4.0F, 12.0F, 7.0F));
		root.addOrReplaceChild("right_front_leg", cubelistbuilder, PartPose.offset(-4.0F, 12.0F, -6.0F));
		root.addOrReplaceChild("left_front_leg", cubelistbuilder, PartPose.offset(4.0F, 12.0F, -6.0F));
		return LayerDefinition.create(mesh, 64, 32);
	}

	@Override
	public void setupAnim(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		if (entityIn.getCowType() == 1)
		{
			this.head.xRot = headPitch / 57.29578F + Mth.cos(limbSwing * 5.6662F + 3.141593F) * 1.4F * (float) (limbSwingAmount != 1.0F ? -1 : 0) + Mth.cos(5.807793F);
			this.head.yRot = netHeadYaw / 57.29578F + Mth.cos(limbSwing * 2.6662F + 3.141593F) * 1.4F * (float) (limbSwingAmount != 1.0F ? -1 : 0) + Mth.cos(5.807793F);
			this.body.xRot = 1.570796F + Mth.cos(limbSwing * 5.6662F) * 1.4F * (float) (limbSwingAmount != 1.0F ? -1 : 0) + Mth.cos(5.807793F);
			this.rightHindLeg.xRot = Mth.cos(limbSwing * 2.6662F) * 1.4F * (float) (limbSwingAmount != 1.0F ? -1 : 0) + Mth.cos(5.807793F);
			this.leftHindLeg.xRot = Mth.cos(limbSwing * 1.6662F + 9.141593F) * 1.4F * (float) (limbSwingAmount != 1.0F ? -1 : 0) + Mth.cos(5.807793F);
			this.rightFrontLeg.xRot = Mth.cos(limbSwing * 2.6662F + 3.141593F) * 1.4F * (float) (limbSwingAmount != 1.0F ? -1 : 0) + Mth.cos(5.807793F);
			this.leftFrontLeg.xRot = Mth.cos(limbSwing * 1.6662F) * 1.4F * (float) (limbSwingAmount != 1.0F ? -1 : 0) + Mth.cos(5.807793F);
		}
		else
		{
			this.head.xRot = headPitch / 57.29578F + Mth.cos(limbSwing * 2.6662F + 3.141593F) * 1.4F * limbSwingAmount;
			this.head.yRot = netHeadYaw / 57.29578F + Mth.cos(limbSwing * 2.6662F + 3.141593F) * 1.4F * limbSwingAmount;
			this.body.xRot = 1.570796F + Mth.cos(limbSwing * 1.6662F) * 1.4F * limbSwingAmount;
			this.rightHindLeg.xRot = Mth.cos(limbSwing * 2.6662F) * 1.4F * limbSwingAmount;
			this.leftHindLeg.xRot = Mth.cos(limbSwing * 1.6662F + 3.141593F) * 1.4F * limbSwingAmount;
			this.rightFrontLeg.xRot = Mth.cos(limbSwing * 2.6662F + 3.141593F) * 1.4F * limbSwingAmount;
			this.leftFrontLeg.xRot = Mth.cos(limbSwing * 1.6662F) * 1.4F * limbSwingAmount;
		}
	}
}