package com.legacy.moolands.world.structure;

import java.util.Optional;

import com.legacy.moolands.registry.MoolandsStructures;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.StructureManager;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraft.world.level.levelgen.structure.StructureType;
import net.minecraft.world.level.levelgen.structure.pieces.PiecesContainer;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePiecesBuilder;

public class MushroomHouseStructure extends Structure
{
	public static final Codec<MushroomHouseStructure> CODEC = Structure.simpleCodec(MushroomHouseStructure::new);

	public MushroomHouseStructure(Structure.StructureSettings settings)
	{
		super(settings);
	}

	private void generatePieces(StructurePiecesBuilder builder, GenerationContext context)
	{
		ChunkPos chunkPos = context.chunkPos();
		RandomSource rand = context.random();
		int samples = 1;
		int y = context.chunkGenerator().getFirstOccupiedHeight(chunkPos.getBlockX(8), chunkPos.getBlockZ(8), Heightmap.Types.WORLD_SURFACE_WG, context.heightAccessor(), context.randomState());
		int width = 5;
		for (int x = 0; x < 1; x++)
		{
			for (int z = 0; z < 1; z++)
			{
				y += context.chunkGenerator().getFirstOccupiedHeight(chunkPos.getBlockX(x * width), chunkPos.getBlockZ(z * width), Heightmap.Types.WORLD_SURFACE_WG, context.heightAccessor(), context.randomState());
				samples++;
			}
		}
		BlockPos pos = new BlockPos(chunkPos.x << 4, y / samples, chunkPos.z << 4);

		if (pos.getY() > 30)
			MushroomHousePieces.assemble(context.structureTemplateManager(), pos.above(2), Rotation.getRandom(rand), builder, rand, context.randomState());
	}

	@Override
	public void afterPlace(WorldGenLevel level, StructureManager structureManager, ChunkGenerator chunkGen, RandomSource rand, BoundingBox bounds, ChunkPos chunkPos, PiecesContainer pieces)
	{
	}

	@Override
	public Optional<GenerationStub> findGenerationPoint(GenerationContext context)
	{
		return onTopOfChunkCenter(context, Heightmap.Types.WORLD_SURFACE_WG, (builder) -> this.generatePieces(builder, context));
	}

	@Override
	public StructureType<?> type()
	{
		return MoolandsStructures.MUSHROOM_HOUSE.getType();
	}
}