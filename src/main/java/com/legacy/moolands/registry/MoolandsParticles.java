package com.legacy.moolands.registry;

import com.legacy.moolands.MoolandsMod;
import com.legacy.moolands.client.particle.MoolandDustParticle;
import com.legacy.moolands.client.particle.data.MoolandDustOptions;
import com.mojang.serialization.Codec;

import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.registries.Registries;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.registries.RegisterEvent;

public class MoolandsParticles
{
	public static final ParticleType<MoolandDustOptions> MOOLAND_DUST = new ParticleType<MoolandDustOptions>(true, MoolandDustOptions.DESERIALIZER)
	{
		@Override
		public Codec<MoolandDustOptions> codec()
		{
			return MoolandDustOptions.CODEC;
		}
	};

	public static void init(RegisterEvent event)
	{
		register(event, "mooland_dust", MOOLAND_DUST);
	}

	private static void register(RegisterEvent event, String key, ParticleType<?> particle)
	{
		event.register(Registries.PARTICLE_TYPE, MoolandsMod.locate(key), () -> particle);
	}

	public static class Register
	{
		@SubscribeEvent
		public static void registerParticleFactories(net.neoforged.neoforge.client.event.RegisterParticleProvidersEvent event)
		{
			event.registerSpriteSet(MOOLAND_DUST, MoolandDustParticle.Factory::new);
		}
	}
}
