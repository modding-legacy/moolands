package com.legacy.moolands.client.render;

import com.legacy.moolands.client.render.models.AwfulCowModel;
import com.legacy.moolands.registry.MoolandsEntityTypes;

import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.neoforge.client.event.EntityRenderersEvent;

public class MoolandEntityRendering
{
	public static void init(IEventBus modBus)
	{
		modBus.addListener(MoolandEntityRendering::initLayers);
		modBus.addListener(MoolandEntityRendering::initRenders);
	}

	public static void initLayers(EntityRenderersEvent.RegisterLayerDefinitions event)
	{
		event.registerLayerDefinition(AwfulCowModel.SADDLE_LAYER, () -> AwfulCowModel.createSaddleLayer(new CubeDeformation(0.5F)));
	}

	public static void initRenders(EntityRenderersEvent.RegisterRenderers event)
	{
		event.registerEntityRenderer(MoolandsEntityTypes.AWFUL_COW, AwfulCowRenderer::new);

	}
}