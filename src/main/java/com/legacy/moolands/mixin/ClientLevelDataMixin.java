package com.legacy.moolands.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

import com.legacy.moolands.registry.MoolandsDimensions;
import com.llamalad7.mixinextras.injector.ModifyReturnValue;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.world.level.LevelHeightAccessor;

@Mixin(ClientLevel.ClientLevelData.class)
public class ClientLevelDataMixin
{
	/*@Inject(at = @At("HEAD"), method = "getHorizonHeight", cancellable = true)
	private void getHorizonHeight(LevelHeightAccessor pLevel, CallbackInfoReturnable<Double> callback)*/
	@ModifyReturnValue(method = "getHorizonHeight", at = @At("RETURN"))
	private double gns$getHorizonHeight(double original, LevelHeightAccessor pLevel)
	{
		if (pLevel instanceof ClientLevel level && level.dimension().equals(MoolandsDimensions.moolandsKey()))
			return 1.0D;

		return original;
	}
}
