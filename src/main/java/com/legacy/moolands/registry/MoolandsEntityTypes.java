package com.legacy.moolands.registry;

import com.legacy.moolands.MoolandsMod;
import com.legacy.moolands.entity.AwfulCowEntity;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.SpawnPlacements;
import net.minecraft.world.level.levelgen.Heightmap;
import net.neoforged.neoforge.event.entity.EntityAttributeCreationEvent;
import net.neoforged.neoforge.event.entity.SpawnPlacementRegisterEvent;
import net.neoforged.neoforge.event.entity.SpawnPlacementRegisterEvent.Operation;
import net.neoforged.neoforge.registries.RegisterEvent;

public class MoolandsEntityTypes
{
	public static final EntityType<AwfulCowEntity> AWFUL_COW = buildEntity("awful_cow", EntityType.Builder.of(AwfulCowEntity::new, MobCategory.CREATURE).sized(0.9F, 1.4F));


	public static void init(RegisterEvent event)
	{
		event.register(Registries.ENTITY_TYPE, MoolandsMod.locate("awful_cow"), () -> AWFUL_COW);
	}

	private static <T extends Entity> EntityType<T> buildEntity(String key, EntityType.Builder<T> builder)
	{
		return builder.build(MoolandsMod.find(key));
	}
	
	public static void registerAttributes(EntityAttributeCreationEvent event)
	{
		event.put(AWFUL_COW, AwfulCowEntity.registerAttributes().build());
	}
	
	public static void registerPlacements(SpawnPlacementRegisterEvent event)
	{
		event.register(MoolandsEntityTypes.AWFUL_COW, SpawnPlacements.Type.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, AwfulCowEntity::spawnConditions, Operation.REPLACE);
	}
}
