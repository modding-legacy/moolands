package com.legacy.moolands.registry;

import java.util.function.Function;

import com.legacy.moolands.MoolandsMod;
import com.legacy.moolands.world.MoolandsSurfaceRuleData;
import com.legacy.moolands.world.gen.StrangeChunkGenerator;
import com.legacy.structure_gel.api.dimension.DimensionAccessHelper;
import com.legacy.structure_gel.api.dimension.DimensionTypeBuilder;
import com.legacy.structure_gel.api.registry.registrar.DimensionRegistrar;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.biome.FixedBiomeSource;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;
import net.minecraft.world.level.levelgen.NoiseSettings;

public class MoolandsDimensions
{
	public static final ResourceLocation MOOLANDS_ID = MoolandsMod.locate("moolands");
	private static final DimensionRegistrar MOOLANDS = create();

	public static void init()
	{
	}

	private static DimensionRegistrar create()
	{
		Function<BootstapContext<?>, DimensionType> dimType = (bootstrap) -> DimensionTypeBuilder.of().minY(0).effects(MOOLANDS_ID).build();
		Function<BootstapContext<?>, NoiseGeneratorSettings> noiseSettings = (bootstrap) -> DimensionAccessHelper.newDimensionSettings(NoiseSettings.create(0, 384, 1, 2), MoolandsBlocks.moo_rock.defaultBlockState(), Blocks.WATER.defaultBlockState(), DimensionAccessHelper.noneNoiseRouter(), MoolandsSurfaceRuleData.create(false), 63, false);
		Function<BootstapContext<?>, ChunkGenerator> chunkGen = (bootstrap) ->
		{
			var noiseGenSettings = bootstrap.lookup(Registries.NOISE_SETTINGS);
			var biomes = bootstrap.lookup(Registries.BIOME);

			return new StrangeChunkGenerator(new FixedBiomeSource(biomes.getOrThrow(MoolandsBiomes.AWKWARD_HEIGHTS.getKey())), noiseGenSettings.getOrThrow(MOOLANDS.getNoiseSettings().getKey()));
		};
		return new DimensionRegistrar(MOOLANDS_ID, dimType, noiseSettings, chunkGen);
	}

	public static ResourceKey<Level> moolandsKey()
	{
		return MOOLANDS.getLevelKey();
	}
}
