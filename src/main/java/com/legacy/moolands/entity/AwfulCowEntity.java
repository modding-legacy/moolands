package com.legacy.moolands.entity;

import com.legacy.moolands.client.particle.data.MoolandDustOptions;
import com.legacy.moolands.registry.MoolandsBlocks;
import com.legacy.moolands.registry.MoolandsDimensions;
import com.legacy.moolands.registry.MoolandsEntityTypes;
import com.legacy.moolands.registry.MoolandsSounds;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.PlayerRideable;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.Saddleable;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.BreedGoal;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.FollowParentGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.PanicGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.ItemUtils;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.phys.Vec3;

public class AwfulCowEntity extends Animal implements PlayerRideable, Saddleable
{
	public static final EntityDataAccessor<Integer> TYPE = SynchedEntityData.defineId(AwfulCowEntity.class, EntityDataSerializers.INT);
	public static final EntityDataAccessor<Boolean> SADDLED = SynchedEntityData.defineId(AwfulCowEntity.class, EntityDataSerializers.BOOLEAN);

	public AwfulCowEntity(EntityType<? extends AwfulCowEntity> type, Level worldIn)
	{
		super(type, worldIn);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(1, new PanicGoal(this, 2.0D));
		this.goalSelector.addGoal(2, new BreedGoal(this, 1.0D));
		this.goalSelector.addGoal(4, new FollowParentGoal(this, 1.25D));
		this.goalSelector.addGoal(5, new WaterAvoidingRandomStrollGoal(this, 1.0D));
		this.goalSelector.addGoal(6, new LookAtPlayerGoal(this, Player.class, 6.0F));
		this.goalSelector.addGoal(7, new RandomLookAroundGoal(this));
	}

	public static AttributeSupplier.Builder registerAttributes()
	{
		return Mob.createMobAttributes().add(Attributes.MAX_HEALTH, 20.0F).add(Attributes.MOVEMENT_SPEED, 0.2F);
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();
		this.entityData.define(TYPE, 0);
		this.entityData.define(SADDLED, false);
	}

	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor world, DifficultyInstance difficulty, MobSpawnType reason, SpawnGroupData livingData, CompoundTag nbt)
	{
		this.setCowType(this.random.nextInt(2));
		return super.finalizeSpawn(world, difficulty, reason, livingData, nbt);
	}

	public static boolean spawnConditions(EntityType<? extends Animal> type, LevelAccessor level, MobSpawnType reason, BlockPos pos, RandomSource rand)
	{
		return level.getBlockState(pos.below()).getBlock() == MoolandsBlocks.mooland_grass_block && level.getRawBrightness(pos, 0) > 8;
	}

	@Override
	public boolean causeFallDamage(float distance, float damageMultiplier, DamageSource source)
	{
		return false;
	}

	public void setCowType(int type)
	{
		this.entityData.set(TYPE, type);
	}

	public int getCowType()
	{
		return this.entityData.get(TYPE);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void tick()
	{
		super.tick();

		this.setMaxUpStep(!this.getPassengers().isEmpty() ? 1F : 0.5F);
		if (!this.getPassengers().isEmpty())
			return;

		if (this.getCowType() == 0)
		{
			if (this.level().isClientSide() && this.onGround() && this.random.nextFloat() < 0.01F)
			{
				float offset = this.random.nextBoolean() ? -0.1F : 0.1F;
				float dist = (this.getBbWidth() + 1.0F) * 0.5F;
				float x = dist * Mth.sin(this.yBodyRot * (Mth.PI / 180F) + offset);
				float z = dist * Mth.cos(this.yBodyRot * (Mth.PI / 180F) + offset);
				this.level().addParticle(ParticleTypes.FALLING_DRIPSTONE_WATER, this.getX() - x, this.getY() + this.getEyeHeight() - 0.32F, this.getZ() + z, 0.0D, 0.0D, 0.0D);
			}
		}
		else if (this.getCowType() == 1)
		{
			if (this.onGround())
			{
				if (this.zza != 0.0F)
				{
					this.jumpFromGround();
				}
			}
			else if (this.zza != 0.0F)
			{
				if ((!this.level().isEmptyBlock(this.blockPosition().below()) || !this.level().isEmptyBlock(this.blockPosition().below(2))) && this.level().isEmptyBlock(this.blockPosition().above(2)) && this.level().isEmptyBlock(this.blockPosition().above()))
				{
					this.setDeltaMovement(this.getDeltaMovement().x(), 0.20000000000000001D, this.getDeltaMovement().z());
				}
			}

			if (this.getDeltaMovement().y() < -0.10000000000000001D)
			{
				this.setDeltaMovement(this.getDeltaMovement().x(), -0.10000000000000001D, this.getDeltaMovement().z());
			}

			// wee
			if (this.level().isClientSide() && !this.onGround())
			{
				var motion = this.getDeltaMovement();
				this.level().addParticle(this.isJonathing() ? MoolandDustOptions.JONATHING_COW : MoolandDustOptions.YELLOW_COW, this.getX() + (this.getRandom().nextDouble() - 0.75D), this.getY() + (this.getRandom().nextDouble() - 0.75D), this.getZ() + (this.getRandom().nextDouble() - 0.75D), motion.x, motion.y, motion.z);
			}
		}
	}

	public boolean isJonathing()
	{
		return this.hasCustomName() && this.getCustomName().getString().equals("Jonathing");
	}

	@Override
	public void handleEntityEvent(byte id)
	{
		/*switch (id)
		{
		case 4:
		{
			break;
		}
		default:
			super.handleEntityEvent(id);
		}*/

		super.handleEntityEvent(id);
	}

	@Override
	protected void dropEquipment()
	{
		super.dropEquipment();

		if (this.isSaddled())
			this.spawnAtLocation(Items.SADDLE);
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return MoolandsSounds.AWFUL_COW_AMBIENT;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource source)
	{
		return MoolandsSounds.AWFUL_COW_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return MoolandsSounds.AWFUL_COW_DEATH;
	}

	@Override
	protected float getSoundVolume()
	{
		// 0.3 before?
		return 1.0F;
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);

		compound.putInt("CowType", this.getCowType());
		compound.putBoolean("Saddled", this.isSaddled());

	}

	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);

		this.setCowType(compound.getInt("CowType"));

		String oldKey = "getSaddled";
		this.setSaddled(compound.contains(oldKey) ? compound.getBoolean(oldKey) : compound.getBoolean("Saddled"));
	}

	@Override
	public AgeableMob getBreedOffspring(ServerLevel level, AgeableMob ageable)
	{
		return MoolandsEntityTypes.AWFUL_COW.create(level);
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn)
	{
		return this.isBaby() ? sizeIn.height * 0.95F : 1.3F;
	}

	@Override
	public InteractionResult mobInteract(Player player, InteractionHand hand)
	{
		ItemStack itemstack = player.getItemInHand(hand);

		if (itemstack.is(Items.BUCKET) && !this.isBaby())
		{
			player.playSound(SoundEvents.COW_MILK, 1.0F, 1.0F);
			ItemStack itemstack1 = ItemUtils.createFilledResult(itemstack, player, Items.MILK_BUCKET.getDefaultInstance());
			player.setItemInHand(hand, itemstack1);
			return InteractionResult.sidedSuccess(this.level().isClientSide());
		}

		InteractionResult result = super.mobInteract(player, hand);

		if (this.getPassengers().isEmpty() && this.isSaddled() && !result.consumesAction())
		{
			if (!player.level().isClientSide())
			{
				player.startRiding(this);

				player.yRotO = this.getYRot();
				player.setYRot(this.getYRot());
			}

			return InteractionResult.SUCCESS;
		}

		return result;
	}

	@Override
	public boolean isSaddleable()
	{
		return this.isAlive() && !this.isBaby();
	}

	@Override
	public void equipSaddle(SoundSource source)
	{
		if (source != null)
			this.level().playSound(null, this, this.getSaddleSoundEvent(), source, 0.5F, 1.0F);

		this.setSaddled(true);
	}

	@Override
	public boolean isSaddled()
	{
		return this.entityData.get(SADDLED);
	}

	@Override
	public SoundEvent getSaddleSoundEvent()
	{
		return SoundEvents.PIG_SADDLE;
	}

	public void setSaddled(boolean saddled)
	{
		this.entityData.set(SADDLED, saddled);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void travel(Vec3 vec)
	{
		if (!this.getPassengers().isEmpty() && this.isSaddled())
		{
			if (this.getFirstPassenger()instanceof Player player)
			{
				boolean hasJumped = player.jumping;

				this.setYRot(player.getYRot());
				this.yRotO = this.getYRot();
				this.setXRot(player.getXRot() * 0.5F);
				this.setRot(this.getYRot(), this.getXRot());
				this.yBodyRot = this.getYRot();
				this.yHeadRot = this.getYRot();
				this.setMaxUpStep(1.0F);

				float forward = player.zza;
				float strafe = player.xxa;

				if (forward <= 0.0F)
					forward *= 0.25F;

				if (this.isControlledByLocalInstance())
				{
					double upward = 0;

					float f = (float) this.getAttribute(Attributes.MOVEMENT_SPEED).getValue() * 0.225F;

					this.setSpeed(f);

					if (hasJumped && this.onGround())
					{
						upward = this.cowMountJumpStrength();

						if (this.hasEffect(MobEffects.JUMP))
							upward += (this.getEffect(MobEffects.JUMP).getAmplifier() + 1) * 0.1F;

						this.setDeltaMovement(this.getDeltaMovement().add(0, upward, 0));

					}

					super.travel(new Vec3(strafe * 0.4F, 0, forward));
				}
				else
					this.setDeltaMovement(Vec3.ZERO);

				this.calculateEntityAnimation(false);
			}
			else
				super.travel(vec);
		}
		else
			super.travel(vec);
	}

	@Override
	public LivingEntity getControllingPassenger()
	{
		return this.getFirstPassenger()instanceof LivingEntity living && this.isSaddled() ? living : null;
	}

	@Override
	public float getSpeed()
	{
		return this.cowMountSpeed();
	}

	private float cowMountSpeed()
	{
		return this.level().dimension().equals(MoolandsDimensions.moolandsKey()) ? 0.7F : 0.4F;
	}

	private double cowMountJumpStrength()
	{
		return this.level().dimension().equals(MoolandsDimensions.moolandsKey()) ? 1.2D : 0.6D;
	}

}
