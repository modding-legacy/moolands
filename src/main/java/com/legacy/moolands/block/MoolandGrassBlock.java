package com.legacy.moolands.block;

import java.util.List;

import com.legacy.moolands.registry.MoolandsBlocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.GrassBlock;
import net.minecraft.world.level.block.SaplingBlock;
import net.minecraft.world.level.block.SnowLayerBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.configurations.RandomPatchConfiguration;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.lighting.LightEngine;
import net.neoforged.neoforge.common.PlantType;

public class MoolandGrassBlock extends GrassBlock
{
	public MoolandGrassBlock(Block.Properties props)
	{
		super(props);
	}

	@Override
	public BlockState updateShape(BlockState state, Direction facing, BlockState facingState, LevelAccessor level, BlockPos pos, BlockPos facingPos)
	{
		Block block = level.getBlockState(pos.above()).getBlock();
		return state.setValue(SNOWY, block == Blocks.SNOW || block == Blocks.SNOW_BLOCK);
	}

	@Override
	public void randomTick(BlockState state, ServerLevel serverLevel, BlockPos pos, RandomSource random)
	{
		Block dirtBlock = MoolandsBlocks.mooland_dirt;

		if (!canBeGrass(state, serverLevel, pos))
		{
			if (!serverLevel.isLoaded(pos))
				return;

			serverLevel.setBlockAndUpdate(pos, dirtBlock.defaultBlockState());
		}
		else
		{
			if (serverLevel.getMaxLocalRawBrightness(pos.above()) >= 9)
			{
				for (int i = 0; i < 4; ++i)
				{
					BlockPos spreadPos = pos.offset(random.nextInt(3) - 1, random.nextInt(5) - 3, random.nextInt(3) - 1);
					Block spreadResult = serverLevel.getBlockState(spreadPos).getBlock() == dirtBlock ? this : null;
					if (spreadResult != null)
					{
						BlockState spreadState = spreadResult.defaultBlockState();
						if (canPropagate(spreadState, serverLevel, spreadPos))
						{
							if (spreadState.hasProperty(SNOWY))
								spreadState = spreadState.setValue(SNOWY, serverLevel.getBlockState(spreadPos.above()).is(Blocks.SNOW));
							serverLevel.setBlockAndUpdate(spreadPos, spreadState);
						}
					}
				}
			}

		}
	}

	private static boolean canBeGrass(BlockState state, LevelReader level, BlockPos pos)
	{
		BlockPos abovePos = pos.above();
		BlockState aboveState = level.getBlockState(abovePos);
		if (aboveState.is(Blocks.SNOW) && aboveState.getValue(SnowLayerBlock.LAYERS) == 1)
			return true;
		else if (aboveState.getFluidState().getAmount() == 8)
			return false;
		else
		{
			int light = LightEngine.getLightBlockInto(level, state, pos, aboveState, abovePos, Direction.UP, aboveState.getLightBlock(level, abovePos));
			return light < level.getMaxLightLevel();
		}
	}

	private static boolean canPropagate(BlockState state, LevelReader level, BlockPos pos)
	{
		BlockPos abovePos = pos.above();
		return canBeGrass(state, level, pos) && !level.getFluidState(abovePos).is(FluidTags.WATER);
	}

	@Override
	public void performBonemeal(ServerLevel level, RandomSource rand, BlockPos pos, BlockState state)
	{
		BlockPos abovePos = pos.above();
		BlockState grassState = this.getTallBlock().defaultBlockState();

		attempt: for (int i = 0; i < 128; ++i)
		{
			BlockPos placePos = abovePos;

			for (int j = 0; j < i / 16; ++j)
			{
				placePos = placePos.offset(rand.nextInt(3) - 1, (rand.nextInt(3) - 1) * rand.nextInt(3) / 2, rand.nextInt(3) - 1);
				if (!level.getBlockState(placePos.below()).is(this) || level.getBlockState(placePos).isCollisionShapeFullBlock(level, placePos))
				{
					continue attempt;
				}
			}

			BlockState blockAtPlace = level.getBlockState(placePos);

			/*if (blockAtPlace.is(grassState.getBlock()) && rand.nextInt(10) == 0)
			{
				((BonemealableBlock) grassState.getBlock()).performBonemeal(level, rand, placePos, blockAtPlace);
			}*/

			if (blockAtPlace.isAir())
			{
				if (rand.nextInt(8) == 0)
				{
					List<ConfiguredFeature<?, ?>> flowerFeatures = level.getBiome(placePos).value().getGenerationSettings().getFlowerFeatures();

					if (flowerFeatures.isEmpty())
						continue;

					PlacedFeature placedFeature = ((RandomPatchConfiguration) flowerFeatures.get(rand.nextInt(flowerFeatures.size())).config()).feature().value();
					placedFeature.place(level, level.getChunkSource().getGenerator(), rand, placePos);
				}
				else
					level.setBlockAndUpdate(placePos, grassState);
			}
		}

	}

	@Override
	public boolean canSustainPlant(BlockState state, BlockGetter world, BlockPos pos, Direction facing, net.neoforged.neoforge.common.IPlantable plantable)
	{
		if (plantable instanceof SaplingBlock)
			return true;

		PlantType plantType = plantable.getPlantType(world, pos.relative(facing));

		if (plantType.equals(PlantType.PLAINS))
			return true;
		else
			return super.canSustainPlant(state, world, pos, facing, plantable);
	}

	// futureproofing
	public Block getTallBlock()
	{
		return MoolandsBlocks.mooland_grass;
	}
}